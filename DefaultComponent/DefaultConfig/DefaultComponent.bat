echo off

set RHAP_JARS_DIR=C:/Program Files (x86)/IBM/Rational/Rhapsody/8.0.3/Share\LangJava\lib
set FRAMEWORK_NONE_JARS=C:/Program Files (x86)/IBM/Rational/Rhapsody/8.0.3/Share\LangJava\lib\oxf.jar;C:/Program Files (x86)/IBM/Rational/Rhapsody/8.0.3/Share\LangJava\lib\anim.jar;C:/Program Files (x86)/IBM/Rational/Rhapsody/8.0.3/Share\LangJava\lib\animcom.jar;C:/Program Files (x86)/IBM/Rational/Rhapsody/8.0.3/Share\LangJava\lib\oxfInstMock.jar;
set FRAMEWORK_ANIM_JARS=C:/Program Files (x86)/IBM/Rational/Rhapsody/8.0.3/Share\LangJava\lib\oxf.jar;C:/Program Files (x86)/IBM/Rational/Rhapsody/8.0.3/Share\LangJava\lib\anim.jar;C:/Program Files (x86)/IBM/Rational/Rhapsody/8.0.3/Share\LangJava\lib\animcom.jar;C:/Program Files (x86)/IBM/Rational/Rhapsody/8.0.3/Share\LangJava\lib\oxfInst.jar;
set SOURCEPATH=%SOURCEPATH%
set CLASSPATH=%CLASSPATH%;.
set PATH=%RHAP_JARS_DIR%;%PATH%;
set INSTRUMENTATION=Animation   

set BUILDSET=Debug

if %INSTRUMENTATION%==Animation goto anim

:noanim
set CLASSPATH=%CLASSPATH%;%FRAMEWORK_NONE_JARS%
goto setEnv_end

:anim
set CLASSPATH=%CLASSPATH%;%FRAMEWORK_ANIM_JARS%

:setEnv_end

if "%1" == "" goto compile
if "%1" == "build" goto compile
if "%1" == "clean" goto clean
if "%1" == "rebuild" goto clean
if "%1" == "run" goto run

:clean
echo cleaning class files
if exist Elevator\evEmergencyStop.class del Elevator\evEmergencyStop.class
if exist Gui\evUnlockB.class del Gui\evUnlockB.class
if exist Elevator\Elevator.class del Elevator\Elevator.class
if exist Elevator\evChangeDirection.class del Elevator\evChangeDirection.class
if exist Gui\evLockA.class del Gui\evLockA.class
if exist Elevator\evCloseNow.class del Elevator\evCloseNow.class
if exist Gui\closeTheDoorNow.class del Gui\closeTheDoorNow.class
if exist MainDefaultComponent.class del MainDefaultComponent.class
if exist Elevator\evMove.class del Elevator\evMove.class
if exist Elevator\Motor.class del Elevator\Motor.class
if exist Elevator\Itinerary.class del Elevator\Itinerary.class
if exist Elevator\Building.class del Elevator\Building.class
if exist Gui\ElevatorGui.class del Gui\ElevatorGui.class
if exist Elevator\evUnlock.class del Elevator\evUnlock.class
if exist Elevator\evKeyPress.class del Elevator\evKeyPress.class
if exist Gui\evLockB.class del Gui\evLockB.class
if exist Elevator\evClosed.class del Elevator\evClosed.class
if exist Gui\evCloseNowB.class del Gui\evCloseNowB.class
if exist Elevator\IHardware.class del Elevator\IHardware.class
if exist Elevator\evEmergencyCancel.class del Elevator\evEmergencyCancel.class
if exist Gui\evCloseNowA.class del Gui\evCloseNowA.class
if exist Elevator\evCall.class del Elevator\evCall.class
if exist Elevator\Elevator_pkgClass.class del Elevator\Elevator_pkgClass.class
if exist Gui\Gui_pkgClass.class del Gui\Gui_pkgClass.class
if exist Elevator\Door.class del Elevator\Door.class
if exist Elevator\evOpen.class del Elevator\evOpen.class
if exist Gui\evUnlockA.class del Gui\evUnlockA.class
if exist Elevator\evLock.class del Elevator\evLock.class
if exist Elevator\evGoto.class del Elevator\evGoto.class
if exist Elevator\Direction.class del Elevator\Direction.class
if exist Elevator\evAtFloor.class del Elevator\evAtFloor.class
if exist Gui\closeTheDoorNowA.class del Gui\closeTheDoorNowA.class

if "%1" == "clean" goto end

:compile   
if %BUILDSET%==Debug goto compile_debug
echo compiling JAVA source files
javac  @files.lst
goto end

:compile_debug
echo compiling JAVA source files
javac -g  @files.lst
goto end

:run

java %2

:end


