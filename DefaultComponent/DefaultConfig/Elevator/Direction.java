/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Direction
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Elevator/Direction.java
*********************************************************************/

package Elevator;


//----------------------------------------------------------------------------
// Elevator/Direction.java                                                                  
//----------------------------------------------------------------------------

//## package Elevator 


//## class Direction 
public enum Direction {
    UP,
    DOWN;
    
    
    
    // Constructors
    
    //## auto_generated 
     Direction() {
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Elevator/Direction.java
*********************************************************************/

