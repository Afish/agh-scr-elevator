/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Door
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Elevator/Door.java
*********************************************************************/

package Elevator;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Elevator/Door.java                                                                  
//----------------------------------------------------------------------------

//## package Elevator 


//## class Door 
public class Door implements RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassDoor = new AnimClass("Elevator.Door",false);
    //#]
    
    public Reactive reactive;		//## ignore 
    
    protected int floor;		//## attribute floor 
    
    protected int id;		//## attribute id 
    
    protected boolean isOpen = false;		//## attribute isOpen 
    
    protected Elevator itsElevator;		//## link itsElevator 
    
    protected IHardware itsHardware;		//## link itsHardware 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int opening=1;
    public static final int open=2;
    public static final int locked=3;
    public static final int closing=4;
    public static final int closed=5;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    public static final int Door_Timeout_opening_id = 1;		//## ignore 
    
    public static final int Door_Timeout_open_id = 2;		//## ignore 
    
    public static final int Door_Timeout_closing_id = 3;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## operation Door() 
    public  Door(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassDoor.getUserClass(),
               new ArgData[] {
               });
        
        reactive = new Reactive(p_thread);
        //#[ operation Door() 
        floor = 0;
        id = 0;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public int getFloor() {
        return floor;
    }
    
    //## auto_generated 
    public void setFloor(int p_floor) {
        floor = p_floor;
    }
    
    //## auto_generated 
    public int getId() {
        return id;
    }
    
    //## auto_generated 
    public void setId(int p_id) {
        id = p_id;
    }
    
    //## auto_generated 
    public boolean getIsOpen() {
        return isOpen;
    }
    
    //## auto_generated 
    public void setIsOpen(boolean p_isOpen) {
        isOpen = p_isOpen;
    }
    
    //## auto_generated 
    public Elevator getItsElevator() {
        return itsElevator;
    }
    
    //## auto_generated 
    public void __setItsElevator(Elevator p_Elevator) {
        itsElevator = p_Elevator;
        if(p_Elevator != null)
            {
                animInstance().notifyRelationAdded("itsElevator", p_Elevator);
            }
        else
            {
                animInstance().notifyRelationCleared("itsElevator");
            }
    }
    
    //## auto_generated 
    public void _setItsElevator(Elevator p_Elevator) {
        if(itsElevator != null)
            {
                itsElevator.__setItsDoor(null);
            }
        __setItsElevator(p_Elevator);
    }
    
    //## auto_generated 
    public void setItsElevator(Elevator p_Elevator) {
        if(p_Elevator != null)
            {
                p_Elevator._setItsDoor(this);
            }
        _setItsElevator(p_Elevator);
    }
    
    //## auto_generated 
    public void _clearItsElevator() {
        animInstance().notifyRelationCleared("itsElevator");
        itsElevator = null;
    }
    
    //## auto_generated 
    public IHardware getItsHardware() {
        return itsHardware;
    }
    
    //## auto_generated 
    public void setItsHardware(IHardware p_IHardware) {
        itsHardware = p_IHardware;
        if(p_IHardware != null)
            {
                animInstance().notifyRelationAdded("itsHardware", p_IHardware);
            }
        else
            {
                animInstance().notifyRelationCleared("itsHardware");
            }
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = false;
        done = reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            switch (rootState_subState) {
                case closed:
                {
                    closed_add(animStates);
                }
                break;
                case opening:
                {
                    opening_add(animStates);
                }
                break;
                case closing:
                {
                    closing_add(animStates);
                }
                break;
                case open:
                {
                    open_add(animStates);
                }
                break;
                case locked:
                {
                    locked_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case closed:
                {
                    res = closed_takeEvent(id);
                }
                break;
                case opening:
                {
                    res = opening_takeEvent(id);
                }
                break;
                case closing:
                {
                    res = closing_takeEvent(id);
                }
                break;
                case open:
                {
                    res = open_takeEvent(id);
                }
                break;
                case locked:
                {
                    res = locked_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void opening_add(AnimStates animStates) {
            animStates.add("ROOT.opening");
        }
        
        //## statechart_method 
        public void open_add(AnimStates animStates) {
            animStates.add("ROOT.open");
        }
        
        //## statechart_method 
        public void locked_add(AnimStates animStates) {
            animStates.add("ROOT.locked");
        }
        
        //## statechart_method 
        public void closing_add(AnimStates animStates) {
            animStates.add("ROOT.closing");
        }
        
        //## statechart_method 
        public void closed_add(AnimStates animStates) {
            animStates.add("ROOT.closed");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
        }
        
        //## statechart_method 
        public void closing_enter() {
            animInstance().notifyStateEntered("ROOT.closing");
            rootState_subState = closing;
            rootState_active = closing;
            closingEnter();
        }
        
        //## statechart_method 
        public void lockedEnter() {
        }
        
        //## statechart_method 
        public int openTakeevCloseNow() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("3");
            open_exit();
            closing_entDef();
            animInstance().notifyTransitionEnded("3");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int openTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Door_Timeout_open_id)
                {
                    animInstance().notifyTransitionStarted("2");
                    open_exit();
                    closing_entDef();
                    animInstance().notifyTransitionEnded("2");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void closedExit() {
        }
        
        //## statechart_method 
        public int closing_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = closingTakeRiJTimeout();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void openingExit() {
            itsRiJThread.unschedTimeout(Door_Timeout_opening_id, this);
        }
        
        //## statechart_method 
        public int closingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Door_Timeout_closing_id)
                {
                    animInstance().notifyTransitionStarted("4");
                    closing_exit();
                    closed_entDef();
                    animInstance().notifyTransitionEnded("4");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void locked_exit() {
            lockedExit();
            animInstance().notifyStateExited("ROOT.locked");
        }
        
        //## statechart_method 
        public int open_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evCloseNow.evCloseNow_Elevator_id))
                {
                    res = openTakeevCloseNow();
                }
            else if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = openTakeRiJTimeout();
                }
            else if(event.isTypeOf(evLock.evLock_Elevator_id))
                {
                    res = openTakeevLock();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int openTakeevLock() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("6");
            open_exit();
            locked_entDef();
            animInstance().notifyTransitionEnded("6");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void closed_entDef() {
            closed_enter();
        }
        
        //## statechart_method 
        public void closingEnter() {
            //#[ state closing.(Entry) 
             itsHardware.closing(id, floor);
            //#]
            itsRiJThread.schedTimeout(1000, Door_Timeout_closing_id, this, "ROOT.closing");
        }
        
        //## statechart_method 
        public void open_entDef() {
            open_enter();
        }
        
        //## statechart_method 
        public void opening_entDef() {
            opening_enter();
        }
        
        //## statechart_method 
        public void openEnter() {
            //#[ state open.(Entry) 
            itsHardware.open(id, floor);
            isOpen = true;
            //#]
            itsRiJThread.schedTimeout(3000, Door_Timeout_open_id, this, "ROOT.open");
        }
        
        //## statechart_method 
        public int openingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Door_Timeout_opening_id)
                {
                    animInstance().notifyTransitionStarted("5");
                    opening_exit();
                    open_entDef();
                    animInstance().notifyTransitionEnded("5");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void closing_exit() {
            closingExit();
            animInstance().notifyStateExited("ROOT.closing");
        }
        
        //## statechart_method 
        public void openExit() {
            itsRiJThread.unschedTimeout(Door_Timeout_open_id, this);
            //#[ state open.(Exit) 
            isOpen = false;
            //#]
        }
        
        //## statechart_method 
        public void openingEnter() {
            //#[ state opening.(Entry) 
            itsHardware.opening(id, floor);
            //#]
            itsRiJThread.schedTimeout(1000, Door_Timeout_opening_id, this, "ROOT.opening");
        }
        
        //## statechart_method 
        public void closedEnter() {
        }
        
        //## statechart_method 
        public void lockedExit() {
        }
        
        //## statechart_method 
        public void locked_enter() {
            animInstance().notifyStateEntered("ROOT.locked");
            rootState_subState = locked;
            rootState_active = locked;
            lockedEnter();
        }
        
        //## statechart_method 
        public void open_exit() {
            openExit();
            animInstance().notifyStateExited("ROOT.open");
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public int closed_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evOpen.evOpen_Elevator_id))
                {
                    res = closedTakeevOpen();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int closedTakeevOpen() {
            evOpen params = (evOpen) event;
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("1");
            closed_exit();
            //#[ transition 1 
            
            id = params.anId;
            floor = params.aFloor;
            //#]
            opening_entDef();
            animInstance().notifyTransitionEnded("1");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void closingExit() {
            itsRiJThread.unschedTimeout(Door_Timeout_closing_id, this);
            //#[ state closing.(Exit) 
            itsHardware.moving(id, floor);
            itsElevator.gen(new evClosed());
            //#]
        }
        
        //## statechart_method 
        public void closing_entDef() {
            closing_enter();
        }
        
        //## statechart_method 
        public void opening_exit() {
            openingExit();
            animInstance().notifyStateExited("ROOT.opening");
        }
        
        //## statechart_method 
        public void closed_exit() {
            closedExit();
            animInstance().notifyStateExited("ROOT.closed");
        }
        
        //## statechart_method 
        public void locked_entDef() {
            locked_enter();
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("0");
            closed_entDef();
            animInstance().notifyTransitionEnded("0");
        }
        
        //## statechart_method 
        public void closed_enter() {
            animInstance().notifyStateEntered("ROOT.closed");
            rootState_subState = closed;
            rootState_active = closed;
            closedEnter();
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public int locked_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evUnlock.evUnlock_Elevator_id))
                {
                    res = lockedTakeevUnlock();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void open_enter() {
            animInstance().notifyStateEntered("ROOT.open");
            rootState_subState = open;
            rootState_active = open;
            openEnter();
        }
        
        //## statechart_method 
        public int opening_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = openingTakeRiJTimeout();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int lockedTakeevUnlock() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("7");
            locked_exit();
            open_entDef();
            animInstance().notifyTransitionEnded("7");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void opening_enter() {
            animInstance().notifyStateEntered("ROOT.opening");
            rootState_subState = opening;
            rootState_active = opening;
            openingEnter();
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return Door.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassDoor; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("floor", floor);
        msg.add("id", id);
        msg.add("isOpen", isOpen);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("itsElevator", false, true, itsElevator);
        msg.add("itsHardware", false, true, itsHardware);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Door.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Door.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Door.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Elevator/Door.java
*********************************************************************/

