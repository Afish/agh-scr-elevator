/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Elevator
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Elevator/Elevator.java
*********************************************************************/

package Elevator;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Elevator/Elevator.java                                                                  
//----------------------------------------------------------------------------

//## package Elevator 


//## class Elevator 
public class Elevator implements RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassElevator = new AnimClass("Elevator.Elevator",false);
    //#]
    
    public Reactive reactive;		//## ignore 
    
    protected int floor;		//## attribute floor 
    
    protected int id;		//## attribute id 
    
    protected Door itsDoor;		//## classInstance itsDoor 
    
    protected Itinerary itsDownItinerary;		//## classInstance itsDownItinerary 
    
    protected IHardware itsHardware;		//## link itsHardware 
    
    protected Itinerary itsItinerary;		//## link itsItinerary 
    
    protected Motor itsMotor;		//## classInstance itsMotor 
    
    protected Itinerary itsUpItinerary;		//## classInstance itsUpItinerary 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int emergencyStop=1;
    public static final int active=2;
    public static final int state_9=3;
    public static final int wait=4;
    public static final int openingDoor=5;
    public static final int moving=6;
    public static final int closingDoor=7;
    public static final int changingDirection=8;
    public static final int state_10=9;
    public static final int direction=10;
    public static final int goingUp=11;
    public static final int goingDown=12;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    protected int state_9_subState;		//## ignore 
    
    protected int state_9_active;		//## ignore 
    
    public static final int Elevator_Timeout_closingDoor_id = 1;		//## ignore 
    
    public static final int Elevator_Timeout_changingDirection_id = 2;		//## ignore 
    
    protected int state_10_subState;		//## ignore 
    
    protected int state_10_active;		//## ignore 
    
    protected int direction_subState;		//## ignore 
    
    protected int direction_lastState;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## operation Elevator() 
    public  Elevator(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassElevator.getUserClass(),
               new ArgData[] {
               });
        
        reactive = new Reactive(p_thread);
        initRelations(p_thread);
        //#[ operation Elevator() 
        floor = 0;
        id = 0;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param aFloor
    */
    //## operation addFloor(int) 
    public void addFloor(int aFloor) {
        try {
            animInstance().notifyMethodEntered("addFloor",
               new ArgData[] {
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor))
               });
        
        //#[ operation addFloor(int) 
        if(aFloor > floor)
        	itsUpItinerary.add(aFloor);
        else
        	itsDownItinerary.add(aFloor);
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation closeTheDoorNow() 
    public void closeTheDoorNow() {
        try {
            animInstance().notifyMethodEntered("closeTheDoorNow",
               new ArgData[] {
               });
        
        //#[ operation closeTheDoorNow() 
        itsDoor.gen(new evCloseNow());
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation isAtFloor() 
    public boolean isAtFloor() {
        try {
            animInstance().notifyMethodEntered("isAtFloor",
               new ArgData[] {
               });
        
        //#[ operation isAtFloor() 
        return (itsItinerary.isAtFloor(floor));
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation isCalledInCurrentDirection() 
    public boolean isCalledInCurrentDirection() {
        try {
            animInstance().notifyMethodEntered("isCalledInCurrentDirection",
               new ArgData[] {
               });
        
        //#[ operation isCalledInCurrentDirection() 
        if(itsItinerary == itsUpItinerary)
        	return (itsItinerary.isNotEmpty(floor, Direction.UP));
        else
        	return (itsItinerary.isNotEmpty(floor, Direction.DOWN));
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation isCalledInOppositeDirection() 
    public boolean isCalledInOppositeDirection() {
        try {
            animInstance().notifyMethodEntered("isCalledInOppositeDirection",
               new ArgData[] {
               });
        
        //#[ operation isCalledInOppositeDirection() 
        if ( itsItinerary == itsUpItinerary )
            return ( itsDownItinerary.isNotEmpty( floor, Direction.DOWN) );
        else
            return ( itsUpItinerary.isNotEmpty( floor, Direction.UP) );
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation lock() 
    public boolean lock() {
        try {
            animInstance().notifyMethodEntered("lock",
               new ArgData[] {
               });
        
        //#[ operation lock() 
        if(itsDoor.isOpen)
        {
        	itsDoor.gen(new evLock()); 
        	return true;
        }
        else
        	return false;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation nextFloor() 
    public void nextFloor() {
        try {
            animInstance().notifyMethodEntered("nextFloor",
               new ArgData[] {
               });
        
        //#[ operation nextFloor() 
        itsHardware.close( id, floor );
        if ( itsItinerary == itsUpItinerary ) {
            floor++;
            itsMotor.gen( new evMove( Direction.UP ) );
        } else {
            --floor;
            itsMotor.gen( new evMove( Direction.DOWN ) );
        }
        floor %= 5;
        itsHardware.moving( id, floor );
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation removeFloor() 
    public void removeFloor() {
        try {
            animInstance().notifyMethodEntered("removeFloor",
               new ArgData[] {
               });
        
        //#[ operation removeFloor() 
        itsItinerary.remove(floor);
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation unlock() 
    public void unlock() {
        try {
            animInstance().notifyMethodEntered("unlock",
               new ArgData[] {
               });
        
        //#[ operation unlock() 
        itsDoor.gen(new evUnlock());
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public int getFloor() {
        return floor;
    }
    
    //## auto_generated 
    public void setFloor(int p_floor) {
        floor = p_floor;
    }
    
    //## auto_generated 
    public int getId() {
        return id;
    }
    
    //## auto_generated 
    public void setId(int p_id) {
        id = p_id;
    }
    
    //## auto_generated 
    public Door getItsDoor() {
        return itsDoor;
    }
    
    //## auto_generated 
    public void __setItsDoor(Door p_Door) {
        itsDoor = p_Door;
        if(p_Door != null)
            {
                animInstance().notifyRelationAdded("itsDoor", p_Door);
            }
        else
            {
                animInstance().notifyRelationCleared("itsDoor");
            }
    }
    
    //## auto_generated 
    public void _setItsDoor(Door p_Door) {
        if(itsDoor != null)
            {
                itsDoor.__setItsElevator(null);
            }
        __setItsDoor(p_Door);
    }
    
    //## auto_generated 
    public Door newItsDoor(RiJThread p_thread) {
        itsDoor = new Door(p_thread);
        itsDoor._setItsElevator(this);
        animInstance().notifyRelationAdded("itsDoor", itsDoor);
        return itsDoor;
    }
    
    //## auto_generated 
    public void deleteItsDoor() {
        itsDoor.__setItsElevator(null);
        animInstance().notifyRelationRemoved("itsDoor", itsDoor);
        itsDoor=null;
    }
    
    //## auto_generated 
    public Itinerary getItsDownItinerary() {
        return itsDownItinerary;
    }
    
    //## auto_generated 
    public Itinerary newItsDownItinerary() {
        itsDownItinerary = new Itinerary();
        animInstance().notifyRelationAdded("itsDownItinerary", itsDownItinerary);
        return itsDownItinerary;
    }
    
    //## auto_generated 
    public void deleteItsDownItinerary() {
        animInstance().notifyRelationRemoved("itsDownItinerary", itsDownItinerary);
        itsDownItinerary=null;
    }
    
    //## auto_generated 
    public IHardware getItsHardware() {
        return itsHardware;
    }
    
    //## auto_generated 
    public void setItsHardware(IHardware p_IHardware) {
        itsHardware = p_IHardware;
        if(p_IHardware != null)
            {
                animInstance().notifyRelationAdded("itsHardware", p_IHardware);
            }
        else
            {
                animInstance().notifyRelationCleared("itsHardware");
            }
    }
    
    //## auto_generated 
    public Itinerary getItsItinerary() {
        return itsItinerary;
    }
    
    //## auto_generated 
    public void setItsItinerary(Itinerary p_Itinerary) {
        itsItinerary = p_Itinerary;
        if(p_Itinerary != null)
            {
                animInstance().notifyRelationAdded("itsItinerary", p_Itinerary);
            }
        else
            {
                animInstance().notifyRelationCleared("itsItinerary");
            }
    }
    
    //## auto_generated 
    public Motor getItsMotor() {
        return itsMotor;
    }
    
    //## auto_generated 
    public void __setItsMotor(Motor p_Motor) {
        itsMotor = p_Motor;
        if(p_Motor != null)
            {
                animInstance().notifyRelationAdded("itsMotor", p_Motor);
            }
        else
            {
                animInstance().notifyRelationCleared("itsMotor");
            }
    }
    
    //## auto_generated 
    public void _setItsMotor(Motor p_Motor) {
        if(itsMotor != null)
            {
                itsMotor.__setItsElevator(null);
            }
        __setItsMotor(p_Motor);
    }
    
    //## auto_generated 
    public Motor newItsMotor(RiJThread p_thread) {
        itsMotor = new Motor(p_thread);
        itsMotor._setItsElevator(this);
        animInstance().notifyRelationAdded("itsMotor", itsMotor);
        return itsMotor;
    }
    
    //## auto_generated 
    public void deleteItsMotor() {
        itsMotor.__setItsElevator(null);
        animInstance().notifyRelationRemoved("itsMotor", itsMotor);
        itsMotor=null;
    }
    
    //## auto_generated 
    public Itinerary getItsUpItinerary() {
        return itsUpItinerary;
    }
    
    //## auto_generated 
    public Itinerary newItsUpItinerary() {
        itsUpItinerary = new Itinerary();
        animInstance().notifyRelationAdded("itsUpItinerary", itsUpItinerary);
        return itsUpItinerary;
    }
    
    //## auto_generated 
    public void deleteItsUpItinerary() {
        animInstance().notifyRelationRemoved("itsUpItinerary", itsUpItinerary);
        itsUpItinerary=null;
    }
    
    //## auto_generated 
    protected void initRelations(RiJThread p_thread) {
        itsDoor = newItsDoor(p_thread);
        itsDownItinerary = newItsDownItinerary();
        itsMotor = newItsMotor(p_thread);
        itsUpItinerary = newItsUpItinerary();
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = true;
        done &= itsDoor.startBehavior();
        done &= itsMotor.startBehavior();
        done &= reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(direction_subState == state)
                {
                    return true;
                }
            if(state_10 == state)
                {
                    return isIn(active);
                }
            if(state_10_subState == state)
                {
                    return true;
                }
            if(state_9 == state)
                {
                    return isIn(active);
                }
            if(state_9_subState == state)
                {
                    return true;
                }
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            switch (rootState_subState) {
                case active:
                {
                    active_add(animStates);
                }
                break;
                case emergencyStop:
                {
                    emergencyStop_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case active:
                {
                    res = active_dispatchEvent(id);
                }
                break;
                case emergencyStop:
                {
                    res = emergencyStop_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void emergencyStop_add(AnimStates animStates) {
            animStates.add("ROOT.emergencyStop");
        }
        
        //## statechart_method 
        public void active_add(AnimStates animStates) {
            animStates.add("ROOT.active");
            state_9_add(animStates);
            state_10_add(animStates);
        }
        
        //## statechart_method 
        public int active_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(state_9_dispatchEvent(id) >= 0)
                {
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                    if(!isIn(active))
                        {
                            return res;
                        }
                }
            if(state_10_dispatchEvent(id) >= 0)
                {
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                    if(!isIn(active))
                        {
                            return res;
                        }
                }
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = active_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void state_9_add(AnimStates animStates) {
            animStates.add("ROOT.active.state_9");
            switch (state_9_subState) {
                case wait:
                {
                    wait_add(animStates);
                }
                break;
                case changingDirection:
                {
                    changingDirection_add(animStates);
                }
                break;
                case moving:
                {
                    moving_add(animStates);
                }
                break;
                case openingDoor:
                {
                    openingDoor_add(animStates);
                }
                break;
                case closingDoor:
                {
                    closingDoor_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public int state_9_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (state_9_active) {
                case wait:
                {
                    res = wait_takeEvent(id);
                }
                break;
                case changingDirection:
                {
                    res = changingDirection_takeEvent(id);
                }
                break;
                case moving:
                {
                    res = moving_takeEvent(id);
                }
                break;
                case openingDoor:
                {
                    res = openingDoor_takeEvent(id);
                }
                break;
                case closingDoor:
                {
                    res = closingDoor_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void wait_add(AnimStates animStates) {
            animStates.add("ROOT.active.state_9.wait");
        }
        
        //## statechart_method 
        public void openingDoor_add(AnimStates animStates) {
            animStates.add("ROOT.active.state_9.openingDoor");
        }
        
        //## statechart_method 
        public void moving_add(AnimStates animStates) {
            animStates.add("ROOT.active.state_9.moving");
        }
        
        //## statechart_method 
        public void closingDoor_add(AnimStates animStates) {
            animStates.add("ROOT.active.state_9.closingDoor");
        }
        
        //## statechart_method 
        public void changingDirection_add(AnimStates animStates) {
            animStates.add("ROOT.active.state_9.changingDirection");
        }
        
        //## statechart_method 
        public void state_10_add(AnimStates animStates) {
            animStates.add("ROOT.active.state_10");
            if(state_10_subState == direction)
                {
                    direction_add(animStates);
                }
        }
        
        //## statechart_method 
        public int state_10_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (state_10_active) {
                case goingUp:
                {
                    res = goingUp_takeEvent(id);
                }
                break;
                case goingDown:
                {
                    res = goingDown_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void direction_add(AnimStates animStates) {
            animStates.add("ROOT.active.state_10.direction");
            switch (direction_subState) {
                case goingUp:
                {
                    goingUp_add(animStates);
                }
                break;
                case goingDown:
                {
                    goingDown_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void goingUp_add(AnimStates animStates) {
            animStates.add("ROOT.active.state_10.direction.goingUp");
        }
        
        //## statechart_method 
        public void goingDown_add(AnimStates animStates) {
            animStates.add("ROOT.active.state_10.direction.goingDown");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
            state_9_subState = RiJNonState;
            state_9_active = RiJNonState;
            state_10_subState = RiJNonState;
            state_10_active = RiJNonState;
            direction_subState = RiJNonState;
            direction_lastState = RiJNonState;
        }
        
        //## statechart_method 
        public void directionExit() {
        }
        
        //## statechart_method 
        public void state_10_entDef() {
            state_10_enter();
            state_10EntDef();
        }
        
        //## statechart_method 
        public int changingDirection_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = changingDirectionTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = state_9_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void changingDirection_exit() {
            changingDirectionExit();
            animInstance().notifyStateExited("ROOT.active.state_9.changingDirection");
        }
        
        //## statechart_method 
        public void changingDirectionExit() {
            itsRiJThread.unschedTimeout(Elevator_Timeout_changingDirection_id, this);
        }
        
        //## statechart_method 
        public void changingDirection_enter() {
            animInstance().notifyStateEntered("ROOT.active.state_9.changingDirection");
            state_9_subState = changingDirection;
            state_9_active = changingDirection;
            changingDirectionEnter();
        }
        
        //## statechart_method 
        public int active_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public int emergencyStopTakeevEmergencyCancel() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("19");
            emergencyStop_exit();
            active_enter();
            state_9_entDef();
            state_10_enter();
            direction_enter();
            direction_entHist();
            animInstance().notifyTransitionEnded("19");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void goingDownExit() {
        }
        
        //## statechart_method 
        public int goingUpTakeevChangeDirection() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("14");
            goingUp_exit();
            goingDown_entDef();
            animInstance().notifyTransitionEnded("14");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void state_10_enter() {
            animInstance().notifyStateEntered("ROOT.active.state_10");
            state_10Enter();
        }
        
        //## statechart_method 
        public int changingDirectionTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Elevator_Timeout_changingDirection_id)
                {
                    animInstance().notifyTransitionStarted("13");
                    changingDirection_exit();
                    wait_entDef();
                    animInstance().notifyTransitionEnded("13");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public int closingDoorTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Elevator_Timeout_closingDoor_id)
                {
                    animInstance().notifyTransitionStarted("10");
                    closingDoor_exit();
                    wait_entDef();
                    animInstance().notifyTransitionEnded("10");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void moving_enter() {
            animInstance().notifyStateEntered("ROOT.active.state_9.moving");
            state_9_subState = moving;
            state_9_active = moving;
            movingEnter();
        }
        
        //## statechart_method 
        public int openingDoorTakeevClosed() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("9");
            openingDoor_exit();
            //#[ transition 9 
            
            removeFloor();
            //#]
            closingDoor_entDef();
            animInstance().notifyTransitionEnded("9");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void state_9_enter() {
            animInstance().notifyStateEntered("ROOT.active.state_9");
            state_9Enter();
        }
        
        //## statechart_method 
        public int directionTakeevGoto() {
            evGoto params = (evGoto) event;
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("16");
            //#[ transition 16 
            addFloor(params.aFloor);
            //#]
            animInstance().notifyTransitionEnded("16");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void closingDoorExit() {
            itsRiJThread.unschedTimeout(Elevator_Timeout_closingDoor_id, this);
        }
        
        //## statechart_method 
        public void movingExit() {
        }
        
        //## statechart_method 
        public void waitEnter() {
        }
        
        //## statechart_method 
        public void state_9_entDef() {
            state_9_enter();
            state_9EntDef();
        }
        
        //## statechart_method 
        public void emergencyStop_entDef() {
            emergencyStop_enter();
        }
        
        //## statechart_method 
        public int state_9_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void state_10EntDef() {
            direction_entDef();
        }
        
        //## statechart_method 
        public void state_9EntDef() {
            animInstance().notifyTransitionStarted("1");
            wait_entDef();
            animInstance().notifyTransitionEnded("1");
        }
        
        //## statechart_method 
        public void emergencyStop_enter() {
            animInstance().notifyStateEntered("ROOT.emergencyStop");
            rootState_subState = emergencyStop;
            rootState_active = emergencyStop;
            emergencyStopEnter();
        }
        
        //## statechart_method 
        public void movingEnter() {
            //#[ state active.state_9.moving.(Entry) 
            nextFloor();
            //#]
        }
        
        //## statechart_method 
        public void openingDoor_enter() {
            animInstance().notifyStateEntered("ROOT.active.state_9.openingDoor");
            state_9_subState = openingDoor;
            state_9_active = openingDoor;
            openingDoorEnter();
        }
        
        //## statechart_method 
        public void openingDoorEnter() {
            //#[ state active.state_9.openingDoor.(Entry) 
            itsDoor.gen(new evOpen(id, floor));
            //#]
        }
        
        //## statechart_method 
        public int waitTakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            //## transition 3 
            if(isCalledInCurrentDirection())
                {
                    //## transition 5 
                    if(isAtFloor())
                        {
                            animInstance().notifyTransitionStarted("2");
                            animInstance().notifyTransitionStarted("3");
                            animInstance().notifyTransitionStarted("5");
                            wait_exit();
                            openingDoor_entDef();
                            animInstance().notifyTransitionEnded("5");
                            animInstance().notifyTransitionEnded("3");
                            animInstance().notifyTransitionEnded("2");
                            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                        }
                    else
                        {
                            animInstance().notifyTransitionStarted("2");
                            animInstance().notifyTransitionStarted("3");
                            animInstance().notifyTransitionStarted("4");
                            wait_exit();
                            moving_entDef();
                            animInstance().notifyTransitionEnded("4");
                            animInstance().notifyTransitionEnded("3");
                            animInstance().notifyTransitionEnded("2");
                            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                        }
                }
            else
                {
                    //## transition 12 
                    if(isCalledInOppositeDirection())
                        {
                            animInstance().notifyTransitionStarted("2");
                            animInstance().notifyTransitionStarted("11");
                            animInstance().notifyTransitionStarted("12");
                            wait_exit();
                            //#[ transition 12 
                            
                            gen(new evChangeDirection());
                            //#]
                            changingDirection_entDef();
                            animInstance().notifyTransitionEnded("12");
                            animInstance().notifyTransitionEnded("11");
                            animInstance().notifyTransitionEnded("2");
                            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                        }
                }
            return res;
        }
        
        //## statechart_method 
        public void wait_exit() {
            popNullConfig();
            waitExit();
            animInstance().notifyStateExited("ROOT.active.state_9.wait");
        }
        
        //## statechart_method 
        public void goingDown_enter() {
            animInstance().notifyStateEntered("ROOT.active.state_10.direction.goingDown");
            direction_subState = goingDown;
            state_10_active = goingDown;
            goingDownEnter();
        }
        
        //## statechart_method 
        public void directionEnter() {
        }
        
        //## statechart_method 
        public void state_10Exit() {
        }
        
        //## statechart_method 
        public void active_entDef() {
            active_enter();
            state_9_entDef();
            state_10_entDef();
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void goingDownEnter() {
            //#[ state active.state_10.direction.goingDown.(Entry) 
            itsItinerary = itsDownItinerary;
            //#]
        }
        
        //## statechart_method 
        public void goingUpEnter() {
            //#[ state active.state_10.direction.goingUp.(Entry) 
            itsItinerary = itsUpItinerary;
            //#]
        }
        
        //## statechart_method 
        public void goingUp_entDef() {
            goingUp_enter();
        }
        
        //## statechart_method 
        public void direction_entDef() {
            direction_enter();
            
            animInstance().notifyTransitionStarted("0");
            goingUp_entDef();
            animInstance().notifyTransitionEnded("0");
        }
        
        //## statechart_method 
        public void goingDown_entDef() {
            goingDown_enter();
        }
        
        //## statechart_method 
        public void changingDirection_entDef() {
            changingDirection_enter();
        }
        
        //## statechart_method 
        public void closingDoor_enter() {
            animInstance().notifyStateEntered("ROOT.active.state_9.closingDoor");
            state_9_subState = closingDoor;
            state_9_active = closingDoor;
            closingDoorEnter();
        }
        
        //## statechart_method 
        public int movingTakeevAtFloor() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            //## transition 7 
            if(isAtFloor())
                {
                    animInstance().notifyTransitionStarted("6");
                    animInstance().notifyTransitionStarted("7");
                    moving_exit();
                    openingDoor_entDef();
                    animInstance().notifyTransitionEnded("7");
                    animInstance().notifyTransitionEnded("6");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            else
                {
                    animInstance().notifyTransitionStarted("6");
                    animInstance().notifyTransitionStarted("8");
                    moving_exit();
                    moving_entDef();
                    animInstance().notifyTransitionEnded("8");
                    animInstance().notifyTransitionEnded("6");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void moving_exit() {
            movingExit();
            animInstance().notifyStateExited("ROOT.active.state_9.moving");
        }
        
        //## statechart_method 
        public void moving_entDef() {
            moving_enter();
        }
        
        //## statechart_method 
        public void openingDoor_exit() {
            openingDoorExit();
            animInstance().notifyStateExited("ROOT.active.state_9.openingDoor");
        }
        
        //## statechart_method 
        public void openingDoorExit() {
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void goingUpExit() {
        }
        
        //## statechart_method 
        public void goingUp_enter() {
            animInstance().notifyStateEntered("ROOT.active.state_10.direction.goingUp");
            direction_subState = goingUp;
            state_10_active = goingUp;
            goingUpEnter();
        }
        
        //## statechart_method 
        public void direction_exit() {
            direction_lastState = direction_subState;
            switch (direction_subState) {
                case goingUp:
                {
                    goingUp_exit();
                    direction_lastState = goingUp;
                }
                break;
                case goingDown:
                {
                    goingDown_exit();
                    direction_lastState = goingDown;
                }
                break;
                default:
                    break;
            }
            direction_subState = RiJNonState;
            directionExit();
            animInstance().notifyStateExited("ROOT.active.state_10.direction");
        }
        
        //## statechart_method 
        public int closingDoor_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = closingDoorTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = state_9_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void state_9Enter() {
        }
        
        //## statechart_method 
        public void activeEnter() {
        }
        
        //## statechart_method 
        public void goingDown_exit() {
            goingDownExit();
            animInstance().notifyStateExited("ROOT.active.state_10.direction.goingDown");
        }
        
        //## statechart_method 
        public int goingUp_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evChangeDirection.evChangeDirection_Elevator_id))
                {
                    res = goingUpTakeevChangeDirection();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = direction_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void goingUp_exit() {
            goingUpExit();
            animInstance().notifyStateExited("ROOT.active.state_10.direction.goingUp");
        }
        
        //## statechart_method 
        public int direction_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evGoto.evGoto_Elevator_id))
                {
                    res = directionTakeevGoto();
                }
            else if(event.isTypeOf(evEmergencyStop.evEmergencyStop_Elevator_id))
                {
                    res = directionTakeevEmergencyStop();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = state_10_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void state_10Enter() {
        }
        
        //## statechart_method 
        public int openingDoor_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evClosed.evClosed_Elevator_id))
                {
                    res = openingDoorTakeevClosed();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = state_9_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void wait_entDef() {
            wait_enter();
        }
        
        //## statechart_method 
        public int goingDown_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evChangeDirection.evChangeDirection_Elevator_id))
                {
                    res = goingDownTakeevChangeDirection();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = direction_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public int wait_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = waitTakeNull();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = state_9_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("17");
            active_entDef();
            animInstance().notifyTransitionEnded("17");
        }
        
        //## statechart_method 
        public void closingDoorEnter() {
            itsRiJThread.schedTimeout(1000, Elevator_Timeout_closingDoor_id, this, "ROOT.active.state_9.closingDoor");
        }
        
        //## statechart_method 
        public void state_9_exit() {
            switch (state_9_subState) {
                case wait:
                {
                    wait_exit();
                }
                break;
                case changingDirection:
                {
                    changingDirection_exit();
                }
                break;
                case moving:
                {
                    moving_exit();
                }
                break;
                case openingDoor:
                {
                    openingDoor_exit();
                }
                break;
                case closingDoor:
                {
                    closingDoor_exit();
                }
                break;
                default:
                    break;
            }
            state_9_subState = RiJNonState;
            state_9Exit();
            animInstance().notifyStateExited("ROOT.active.state_9");
        }
        
        //## statechart_method 
        public void active_exit() {
            state_9_exit();
            state_10_exit();
            activeExit();
            animInstance().notifyStateExited("ROOT.active");
        }
        
        //## statechart_method 
        public void activeExit() {
        }
        
        //## statechart_method 
        public void emergencyStopEnter() {
        }
        
        //## statechart_method 
        public void direction_entHist() {
            if(direction_lastState != RiJNonState)
                {
                    direction_subState = direction_lastState;
                    switch (direction_subState) {
                        case goingUp:
                        {
                            goingUp_enter();
                        }
                        break;
                        case goingDown:
                        {
                            goingDown_enter();
                        }
                        break;
                        default:
                            break;
                    }
                }
        }
        
        //## statechart_method 
        public int state_10_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void state_10_exit() {
            if(state_10_subState == direction)
                {
                    direction_exit();
                }
            state_10_subState = RiJNonState;
            state_10Exit();
            animInstance().notifyStateExited("ROOT.active.state_10");
        }
        
        //## statechart_method 
        public void active_enter() {
            animInstance().notifyStateEntered("ROOT.active");
            rootState_subState = active;
            rootState_active = active;
            activeEnter();
        }
        
        //## statechart_method 
        public int goingDownTakeevChangeDirection() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("15");
            goingDown_exit();
            goingUp_entDef();
            animInstance().notifyTransitionEnded("15");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int directionTakeevEmergencyStop() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("18");
            active_exit();
            emergencyStop_entDef();
            animInstance().notifyTransitionEnded("18");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void direction_enter() {
            animInstance().notifyStateEntered("ROOT.active.state_10.direction");
            state_10_subState = direction;
            directionEnter();
        }
        
        //## statechart_method 
        public void changingDirectionEnter() {
            itsRiJThread.schedTimeout(20, Elevator_Timeout_changingDirection_id, this, "ROOT.active.state_9.changingDirection");
        }
        
        //## statechart_method 
        public void closingDoor_entDef() {
            closingDoor_enter();
        }
        
        //## statechart_method 
        public void waitExit() {
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public void closingDoor_exit() {
            closingDoorExit();
            animInstance().notifyStateExited("ROOT.active.state_9.closingDoor");
        }
        
        //## statechart_method 
        public int moving_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evAtFloor.evAtFloor_Elevator_id))
                {
                    res = movingTakeevAtFloor();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = state_9_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public int emergencyStop_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evEmergencyCancel.evEmergencyCancel_Elevator_id))
                {
                    res = emergencyStopTakeevEmergencyCancel();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void emergencyStop_exit() {
            emergencyStopExit();
            animInstance().notifyStateExited("ROOT.emergencyStop");
        }
        
        //## statechart_method 
        public void openingDoor_entDef() {
            openingDoor_enter();
        }
        
        //## statechart_method 
        public void wait_enter() {
            animInstance().notifyStateEntered("ROOT.active.state_9.wait");
            pushNullConfig();
            state_9_subState = wait;
            state_9_active = wait;
            waitEnter();
        }
        
        //## statechart_method 
        public void state_9Exit() {
        }
        
        //## statechart_method 
        public void emergencyStopExit() {
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return Elevator.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassElevator; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("floor", floor);
        msg.add("id", id);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("itsDoor", true, true, itsDoor);
        msg.add("itsDownItinerary", true, true, itsDownItinerary);
        msg.add("itsUpItinerary", true, true, itsUpItinerary);
        msg.add("itsMotor", true, true, itsMotor);
        msg.add("itsHardware", false, true, itsHardware);
        msg.add("itsItinerary", false, true, itsItinerary);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Elevator.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Elevator.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Elevator.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Elevator/Elevator.java
*********************************************************************/

