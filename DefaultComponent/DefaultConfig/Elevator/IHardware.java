/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: IHardware
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Elevator/IHardware.java
*********************************************************************/

package Elevator;


//----------------------------------------------------------------------------
// Elevator/IHardware.java                                                                  
//----------------------------------------------------------------------------

//## package Elevator 


//## class IHardware 
public interface IHardware {
    
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation check(int,int) 
    void check(int anElevator, int aFloor);
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation close(int,int) 
    void close(int anElevator, int aFloor);
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation closing(int,int) 
    void closing(int anElevator, int aFloor);
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation moving(int,int) 
    void moving(int anElevator, int aFloor);
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation open(int,int) 
    void open(int anElevator, int aFloor);
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation opening(int,int) 
    void opening(int anElevator, int aFloor);
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Elevator/IHardware.java
*********************************************************************/

