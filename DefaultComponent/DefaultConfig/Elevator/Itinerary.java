/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Itinerary
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Elevator/Itinerary.java
*********************************************************************/

package Elevator;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Elevator/Itinerary.java                                                                  
//----------------------------------------------------------------------------

//## package Elevator 


//## class Itinerary 
public class Itinerary implements Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassItinerary = new AnimClass("Elevator.Itinerary",false);
    //#]
    
    protected final int[] cBYTE = { 0x1, 0x2, 0x4, 0x8, 0x10 };		//## attribute cBYTE 
    
    protected int floor;		//## attribute floor 
    
    
    // Constructors
    
    //## operation Itinerary() 
    public  Itinerary() {
         floor = 0;;
        try {
            animInstance().notifyConstructorEntered(animClassItinerary.getUserClass(),
               new ArgData[] {
               });
        
        //#[ operation Itinerary() 
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param aFloor
    */
    //## operation add(int) 
    public void add(int aFloor) {
        try {
            animInstance().notifyMethodEntered("add",
               new ArgData[] {
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor))
               });
        
        //#[ operation add(int) 
        floor |= cBYTE[aFloor];
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param aFloor
    */
    //## operation isAtFloor(int) 
    public boolean isAtFloor(int aFloor) {
        try {
            animInstance().notifyMethodEntered("isAtFloor",
               new ArgData[] {
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor))
               });
        
        //#[ operation isAtFloor(int) 
        return ( (floor & cBYTE[aFloor]) != 0 );
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param aFloor
     * @param aDirection
    */
    //## operation isNotEmpty(int,Direction) 
    public boolean isNotEmpty(int aFloor, final Direction aDirection) {
        try {
            animInstance().notifyMethodEntered("isNotEmpty",
               new ArgData[] {
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor)),
                   new ArgData(Direction.class, "aDirection", AnimInstance.animToString(aDirection))
               });
        
        //#[ operation isNotEmpty(int,Direction) 
        if ( aDirection == Direction.UP )
                return ( floor > 0 && floor >= cBYTE[aFloor] );
        else
                return ( floor > 0 && floor <= cBYTE[aFloor] );
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param aFloor
    */
    //## operation remove(int) 
    public void remove(int aFloor) {
        try {
            animInstance().notifyMethodEntered("remove",
               new ArgData[] {
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor))
               });
        
        //#[ operation remove(int) 
        floor &= ~cBYTE[aFloor];
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public final int getCBYTE(int i1) {
        return cBYTE[i1];
    }
    
    //## auto_generated 
    public void setCBYTE(int i1, final int p_cBYTE) {
        cBYTE[i1] = p_cBYTE;
    }
    
    //## auto_generated 
    public int getFloor() {
        return floor;
    }
    
    //## auto_generated 
    public void setFloor(int p_floor) {
        floor = p_floor;
    }
    
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassItinerary; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("floor", floor);
        msg.add("cBYTE", cBYTE);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Itinerary.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Itinerary.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Itinerary.this.addRelations(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Elevator/Itinerary.java
*********************************************************************/

