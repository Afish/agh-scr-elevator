/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evAtFloor
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Elevator/evAtFloor.java
*********************************************************************/

package Elevator;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Elevator/evAtFloor.java                                                                  
//----------------------------------------------------------------------------

//## package Elevator 


//## event evAtFloor() 
public class evAtFloor extends RiJEvent implements AnimatedEvent {
    
    public static final int evAtFloor_Elevator_id = 12818;		//## ignore 
    
    
    // Constructors
    
    public  evAtFloor() {
        lId = evAtFloor_Elevator_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evAtFloor_Elevator_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Elevator.evAtFloor");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="evAtFloor(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Elevator/evAtFloor.java
*********************************************************************/

