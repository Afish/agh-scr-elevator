/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evLock
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Elevator/evLock.java
*********************************************************************/

package Elevator;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Elevator/evLock.java                                                                  
//----------------------------------------------------------------------------

//## package Elevator 


//## event evLock() 
public class evLock extends RiJEvent implements AnimatedEvent {
    
    public static final int evLock_Elevator_id = 12827;		//## ignore 
    
    
    // Constructors
    
    public  evLock() {
        lId = evLock_Elevator_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evLock_Elevator_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Elevator.evLock");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="evLock(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Elevator/evLock.java
*********************************************************************/

