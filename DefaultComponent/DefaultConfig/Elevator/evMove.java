/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evMove
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Elevator/evMove.java
*********************************************************************/

package Elevator;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Elevator/evMove.java                                                                  
//----------------------------------------------------------------------------

//## package Elevator 


//## event evMove(Direction) 
public class evMove extends RiJEvent implements AnimatedEvent {
    
    public static final int evMove_Elevator_id = 12817;		//## ignore 
    
    public Direction aDirection;
    
    // Constructors
    
    public  evMove() {
        lId = evMove_Elevator_id;
    }
    public  evMove(Direction p_aDirection) {
        lId = evMove_Elevator_id;
        aDirection = p_aDirection;
    }
    
    public boolean isTypeOf(long id) {
        return (evMove_Elevator_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Elevator.evMove");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
          msg.add("aDirection", aDirection);
    }
    public String toString() {
          String s="evMove(";      
          s += "aDirection=" + AnimInstance.animToString(aDirection) + " ";
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Elevator/evMove.java
*********************************************************************/

