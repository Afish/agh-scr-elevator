/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evOpen
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Elevator/evOpen.java
*********************************************************************/

package Elevator;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Elevator/evOpen.java                                                                  
//----------------------------------------------------------------------------

//## package Elevator 


//## event evOpen(int,int) 
public class evOpen extends RiJEvent implements AnimatedEvent {
    
    public static final int evOpen_Elevator_id = 12816;		//## ignore 
    
    public int anId;
    public int aFloor;
    
    // Constructors
    
    public  evOpen() {
        lId = evOpen_Elevator_id;
    }
    public  evOpen(int p_anId, int p_aFloor) {
        lId = evOpen_Elevator_id;
        anId = p_anId;
        aFloor = p_aFloor;
    }
    
    public boolean isTypeOf(long id) {
        return (evOpen_Elevator_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Elevator.evOpen");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
          msg.add("anId", anId);
          msg.add("aFloor", aFloor);
    }
    public String toString() {
          String s="evOpen(";      
          s += "anId=" + AnimInstance.animToString(anId) + " ";
          s += "aFloor=" + AnimInstance.animToString(aFloor) + " ";
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Elevator/evOpen.java
*********************************************************************/

