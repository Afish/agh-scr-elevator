/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Administrator
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Building
//!	Generated Date	: Wed, 20, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/Building.java
*********************************************************************/

package ElevatorPkg;

//## auto_generated
import java.util.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// ElevatorPkg/Building.java                                                                  
//----------------------------------------------------------------------------

//## package ElevatorPkg 


//## class Building 
public class Building implements RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassBuilding = new AnimClass("ElevatorPkg.Building",false);
    //#]
    
    public Reactive reactive;		//## ignore 
    
    protected Display itsDisplay;		//## classInstance itsDisplay 
    
    protected IHardware itsHardware;		//## link itsHardware 
    
    protected ArrayList<Elevator> theElevator = theElevator = new ArrayList<Elevator>();		//## classInstance theElevator 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int normal=1;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## operation Building() 
    public  Building(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassBuilding.getUserClass(),
               new ArgData[] {
               });
        
        reactive = new Reactive(p_thread);
        initRelations(p_thread);
        //#[ operation Building() 
        for(int i=0;i<2;++i){
        	theElevator.get(i).setId(i);
        }
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param aHardware
    */
    //## operation configure(IHardware) 
    public void configure(final IHardware aHardware) {
        try {
            animInstance().notifyMethodEntered("configure",
               new ArgData[] {
                   new ArgData(IHardware.class, "aHardware", AnimInstance.animToString(aHardware))
               });
        
        //#[ operation configure(IHardware) 
        setItsHardware(aHardware);
        for(int i=0;i<2;++i){
        	theElevator.get(i).setItsHardware(aHardware);
        	theElevator.get(i).getItsDoor().setItsHardware(aHardware);
        }
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param aFloor
     * @param aDirection
    */
    //## operation dispatch(int,Direction) 
    public void dispatch(int aFloor, final Direction aDirection) {
        try {
            animInstance().notifyMethodEntered("dispatch",
               new ArgData[] {
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor)),
                   new ArgData(Direction.class, "aDirection", AnimInstance.animToString(aDirection))
               });
        
        //#[ operation dispatch(int,Direction) 
        if ( aDirection == Direction.UP )
            theElevator.get(0).gen(new evGoto(aFloor));
        else
            theElevator.get(1).gen(new evGoto(aFloor));
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public Display getItsDisplay() {
        return itsDisplay;
    }
    
    //## auto_generated 
    public void __setItsDisplay(Display p_Display) {
        itsDisplay = p_Display;
        if(p_Display != null)
            {
                animInstance().notifyRelationAdded("itsDisplay", p_Display);
            }
        else
            {
                animInstance().notifyRelationCleared("itsDisplay");
            }
    }
    
    //## auto_generated 
    public void _setItsDisplay(Display p_Display) {
        if(itsDisplay != null)
            {
                itsDisplay.__setItsBuilding(null);
            }
        __setItsDisplay(p_Display);
    }
    
    //## auto_generated 
    public Display newItsDisplay(RiJThread p_thread) {
        itsDisplay = new Display(p_thread);
        itsDisplay._setItsBuilding(this);
        animInstance().notifyRelationAdded("itsDisplay", itsDisplay);
        return itsDisplay;
    }
    
    //## auto_generated 
    public void deleteItsDisplay() {
        itsDisplay.__setItsBuilding(null);
        animInstance().notifyRelationRemoved("itsDisplay", itsDisplay);
        itsDisplay=null;
    }
    
    //## auto_generated 
    public IHardware getItsHardware() {
        return itsHardware;
    }
    
    //## auto_generated 
    public void setItsHardware(IHardware p_IHardware) {
        itsHardware = p_IHardware;
        if(p_IHardware != null)
            {
                animInstance().notifyRelationAdded("itsHardware", p_IHardware);
            }
        else
            {
                animInstance().notifyRelationCleared("itsHardware");
            }
    }
    
    //## auto_generated 
    public ListIterator<Elevator> getTheElevator() {
        ListIterator<Elevator> iter = theElevator.listIterator();
        return iter;
    }
    
    //## auto_generated 
    public Elevator newTheElevator() {
        Elevator newElevator = new Elevator(getThread());
        theElevator.add(newElevator);
        animInstance().notifyRelationAdded("theElevator", newElevator);
        return newElevator;
    }
    
    //## auto_generated 
    public void deleteTheElevator(Elevator p_Elevator) {
        theElevator.remove(p_Elevator);
        animInstance().notifyRelationRemoved("theElevator", p_Elevator);
        p_Elevator=null;
    }
    
    //## auto_generated 
    protected void initRelations(RiJThread p_thread) {
        itsDisplay = newItsDisplay(p_thread);
        {
            for (int i = 0; i < 2; i++) 
                newTheElevator();
            
        }
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = true;
        done &= itsDisplay.startBehavior();
        {
            ListIterator<Elevator> iter = theElevator.listIterator();
            while (iter.hasNext()){
                done &= (theElevator.get(iter.nextIndex())).startBehavior();
                iter.next();
            }
        }
        done &= reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            if(rootState_subState == normal)
                {
                    normal_add(animStates);
                }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(rootState_active == normal)
                {
                    res = normal_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void normal_add(AnimStates animStates) {
            animStates.add("ROOT.normal");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
        }
        
        //## statechart_method 
        public int normal_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evCall.evCall_ElevatorPkg_id))
                {
                    res = normalTakeevCall();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void normalExit() {
        }
        
        //## statechart_method 
        public int normalTakeevCall() {
            evCall params = (evCall) event;
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("1");
            normal_exit();
            //#[ transition 1 
            
            dispatch(params.aFloor, params.aDirection);
            //#]
            normal_entDef();
            animInstance().notifyTransitionEnded("1");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void normalEnter() {
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void normal_exit() {
            normalExit();
            animInstance().notifyStateExited("ROOT.normal");
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("0");
            normal_entDef();
            animInstance().notifyTransitionEnded("0");
        }
        
        //## statechart_method 
        public void normal_entDef() {
            normal_enter();
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public void normal_enter() {
            animInstance().notifyStateEntered("ROOT.normal");
            rootState_subState = normal;
            rootState_active = normal;
            normalEnter();
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return Building.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassBuilding; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("theElevator", true, false, theElevator);
        msg.add("itsHardware", false, true, itsHardware);
        msg.add("itsDisplay", true, true, itsDisplay);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Building.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Building.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Building.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/Building.java
*********************************************************************/

