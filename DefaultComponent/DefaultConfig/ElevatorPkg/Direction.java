/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Administrator
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Direction
//!	Generated Date	: Wed, 20, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/Direction.java
*********************************************************************/

package ElevatorPkg;


//----------------------------------------------------------------------------
// ElevatorPkg/Direction.java                                                                  
//----------------------------------------------------------------------------

//## package ElevatorPkg 


//## class Direction 
public enum Direction {
    UP,
    DOWN;
    
    
    
    // Constructors
    
    //## auto_generated 
     Direction() {
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/Direction.java
*********************************************************************/

