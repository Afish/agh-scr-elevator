/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Administrator
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: Motor
//!	Generated Date	: Wed, 20, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/Motor.java
*********************************************************************/

package ElevatorPkg;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// ElevatorPkg/Motor.java                                                                  
//----------------------------------------------------------------------------

//## package ElevatorPkg 


//## class Motor 
public class Motor implements RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassMotor = new AnimClass("ElevatorPkg.Motor",false);
    //#]
    
    public Reactive reactive;		//## ignore 
    
    protected Elevator itsElevator;		//## link itsElevator 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int stationary=1;
    public static final int moving=2;
    public static final int up=3;
    public static final int down=4;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    protected int moving_subState;		//## ignore 
    
    public static final int Motor_Timeout_moving_id = 1;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## auto_generated 
    public  Motor(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassMotor.getUserClass(),
               new ArgData[] {
               });
        
        reactive = new Reactive(p_thread);
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public Elevator getItsElevator() {
        return itsElevator;
    }
    
    //## auto_generated 
    public void __setItsElevator(Elevator p_Elevator) {
        itsElevator = p_Elevator;
        if(p_Elevator != null)
            {
                animInstance().notifyRelationAdded("itsElevator", p_Elevator);
            }
        else
            {
                animInstance().notifyRelationCleared("itsElevator");
            }
    }
    
    //## auto_generated 
    public void _setItsElevator(Elevator p_Elevator) {
        if(itsElevator != null)
            {
                itsElevator.__setItsMotor(null);
            }
        __setItsElevator(p_Elevator);
    }
    
    //## auto_generated 
    public void setItsElevator(Elevator p_Elevator) {
        if(p_Elevator != null)
            {
                p_Elevator._setItsMotor(this);
            }
        _setItsElevator(p_Elevator);
    }
    
    //## auto_generated 
    public void _clearItsElevator() {
        animInstance().notifyRelationCleared("itsElevator");
        itsElevator = null;
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = false;
        done = reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(moving_subState == state)
                {
                    return true;
                }
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            switch (rootState_subState) {
                case stationary:
                {
                    stationary_add(animStates);
                }
                break;
                case moving:
                {
                    moving_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case stationary:
                {
                    res = stationary_takeEvent(id);
                }
                break;
                case up:
                {
                    res = up_takeEvent(id);
                }
                break;
                case down:
                {
                    res = down_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void stationary_add(AnimStates animStates) {
            animStates.add("ROOT.stationary");
        }
        
        //## statechart_method 
        public void moving_add(AnimStates animStates) {
            animStates.add("ROOT.moving");
            switch (moving_subState) {
                case up:
                {
                    up_add(animStates);
                }
                break;
                case down:
                {
                    down_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void up_add(AnimStates animStates) {
            animStates.add("ROOT.moving.up");
        }
        
        //## statechart_method 
        public void down_add(AnimStates animStates) {
            animStates.add("ROOT.moving.down");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
            moving_subState = RiJNonState;
        }
        
        //## statechart_method 
        public void moving_enter() {
            animInstance().notifyStateEntered("ROOT.moving");
            rootState_subState = moving;
            movingEnter();
        }
        
        //## statechart_method 
        public void down_exit() {
            downExit();
            animInstance().notifyStateExited("ROOT.moving.down");
        }
        
        //## statechart_method 
        public void movingExit() {
            itsRiJThread.unschedTimeout(Motor_Timeout_moving_id, this);
        }
        
        //## statechart_method 
        public int stationaryTakeevMove() {
            evMove params = (evMove) event;
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            //## transition 2 
            if(params.aDirection == Direction.DOWN)
                {
                    animInstance().notifyTransitionStarted("1");
                    animInstance().notifyTransitionStarted("2");
                    stationary_exit();
                    moving_enter();
                    down_entDef();
                    animInstance().notifyTransitionEnded("2");
                    animInstance().notifyTransitionEnded("1");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            else
                {
                    animInstance().notifyTransitionStarted("1");
                    animInstance().notifyTransitionStarted("3");
                    stationary_exit();
                    moving_enter();
                    up_entDef();
                    animInstance().notifyTransitionEnded("3");
                    animInstance().notifyTransitionEnded("1");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void stationary_entDef() {
            stationary_enter();
        }
        
        //## statechart_method 
        public void down_enter() {
            animInstance().notifyStateEntered("ROOT.moving.down");
            moving_subState = down;
            rootState_active = down;
            downEnter();
        }
        
        //## statechart_method 
        public void upExit() {
        }
        
        //## statechart_method 
        public void movingEnter() {
            itsRiJThread.schedTimeout(2000, Motor_Timeout_moving_id, this, "ROOT.moving");
        }
        
        //## statechart_method 
        public void upEnter() {
        }
        
        //## statechart_method 
        public void stationaryExit() {
        }
        
        //## statechart_method 
        public void stationary_enter() {
            animInstance().notifyStateEntered("ROOT.stationary");
            rootState_subState = stationary;
            rootState_active = stationary;
            stationaryEnter();
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public int down_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            res = moving_takeEvent(id);
            return res;
        }
        
        //## statechart_method 
        public void downExit() {
        }
        
        //## statechart_method 
        public void moving_exit() {
            switch (moving_subState) {
                case up:
                {
                    up_exit();
                }
                break;
                case down:
                {
                    down_exit();
                }
                break;
                default:
                    break;
            }
            moving_subState = RiJNonState;
            movingExit();
            animInstance().notifyStateExited("ROOT.moving");
        }
        
        //## statechart_method 
        public void moving_entDef() {
            moving_enter();
            
            animInstance().notifyTransitionStarted("4");
            up_entDef();
            animInstance().notifyTransitionEnded("4");
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public int up_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            res = moving_takeEvent(id);
            return res;
        }
        
        //## statechart_method 
        public void up_exit() {
            upExit();
            animInstance().notifyStateExited("ROOT.moving.up");
        }
        
        //## statechart_method 
        public int movingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Motor_Timeout_moving_id)
                {
                    animInstance().notifyTransitionStarted("0");
                    moving_exit();
                    //#[ transition 0 
                    
                    itsElevator.gen(new evAtFloor());
                    //#]
                    stationary_entDef();
                    animInstance().notifyTransitionEnded("0");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void stationaryEnter() {
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("5");
            stationary_entDef();
            animInstance().notifyTransitionEnded("5");
        }
        
        //## statechart_method 
        public int stationary_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evMove.evMove_ElevatorPkg_id))
                {
                    res = stationaryTakeevMove();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void stationary_exit() {
            stationaryExit();
            animInstance().notifyStateExited("ROOT.stationary");
        }
        
        //## statechart_method 
        public void down_entDef() {
            down_enter();
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public void up_enter() {
            animInstance().notifyStateEntered("ROOT.moving.up");
            moving_subState = up;
            rootState_active = up;
            upEnter();
        }
        
        //## statechart_method 
        public void up_entDef() {
            up_enter();
        }
        
        //## statechart_method 
        public int moving_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = movingTakeRiJTimeout();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void downEnter() {
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return Motor.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassMotor; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("itsElevator", false, true, itsElevator);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Motor.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Motor.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Motor.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/Motor.java
*********************************************************************/

