/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Administrator
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evCall
//!	Generated Date	: Wed, 20, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evCall.java
*********************************************************************/

package ElevatorPkg;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// ElevatorPkg/evCall.java                                                                  
//----------------------------------------------------------------------------

//## package ElevatorPkg 


//## event evCall(int,Direction) 
public class evCall extends RiJEvent implements AnimatedEvent {
    
    public static final int evCall_ElevatorPkg_id = 12821;		//## ignore 
    
    public int aFloor;
    public Direction aDirection;
    
    // Constructors
    
    public  evCall() {
        lId = evCall_ElevatorPkg_id;
    }
    public  evCall(int p_aFloor, Direction p_aDirection) {
        lId = evCall_ElevatorPkg_id;
        aFloor = p_aFloor;
        aDirection = p_aDirection;
    }
    
    public boolean isTypeOf(long id) {
        return (evCall_ElevatorPkg_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("ElevatorPkg.evCall");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
          msg.add("aFloor", aFloor);
          msg.add("aDirection", aDirection);
    }
    public String toString() {
          String s="evCall(";      
          s += "aFloor=" + AnimInstance.animToString(aFloor) + " ";
          s += "aDirection=" + AnimInstance.animToString(aDirection) + " ";
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evCall.java
*********************************************************************/

