/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Administrator
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evChangeDirection
//!	Generated Date	: Wed, 20, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evChangeDirection.java
*********************************************************************/

package ElevatorPkg;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// ElevatorPkg/evChangeDirection.java                                                                  
//----------------------------------------------------------------------------

//## package ElevatorPkg 


//## event evChangeDirection() 
public class evChangeDirection extends RiJEvent implements AnimatedEvent {
    
    public static final int evChangeDirection_ElevatorPkg_id = 12820;		//## ignore 
    
    
    // Constructors
    
    public  evChangeDirection() {
        lId = evChangeDirection_ElevatorPkg_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evChangeDirection_ElevatorPkg_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("ElevatorPkg.evChangeDirection");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="evChangeDirection(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evChangeDirection.java
*********************************************************************/

