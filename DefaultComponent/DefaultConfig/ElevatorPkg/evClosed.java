/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Administrator
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evClosed
//!	Generated Date	: Wed, 20, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evClosed.java
*********************************************************************/

package ElevatorPkg;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// ElevatorPkg/evClosed.java                                                                  
//----------------------------------------------------------------------------

//## package ElevatorPkg 


//## event evClosed() 
public class evClosed extends RiJEvent implements AnimatedEvent {
    
    public static final int evClosed_ElevatorPkg_id = 12819;		//## ignore 
    
    
    // Constructors
    
    public  evClosed() {
        lId = evClosed_ElevatorPkg_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evClosed_ElevatorPkg_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("ElevatorPkg.evClosed");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="evClosed(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evClosed.java
*********************************************************************/

