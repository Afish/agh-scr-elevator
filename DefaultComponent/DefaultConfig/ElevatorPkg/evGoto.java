/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Administrator
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evGoto
//!	Generated Date	: Wed, 20, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evGoto.java
*********************************************************************/

package ElevatorPkg;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// ElevatorPkg/evGoto.java                                                                  
//----------------------------------------------------------------------------

//## package ElevatorPkg 


//## event evGoto(int) 
public class evGoto extends RiJEvent implements AnimatedEvent {
    
    public static final int evGoto_ElevatorPkg_id = 12822;		//## ignore 
    
    public int aFloor;
    
    // Constructors
    
    public  evGoto() {
        lId = evGoto_ElevatorPkg_id;
    }
    public  evGoto(int p_aFloor) {
        lId = evGoto_ElevatorPkg_id;
        aFloor = p_aFloor;
    }
    
    public boolean isTypeOf(long id) {
        return (evGoto_ElevatorPkg_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("ElevatorPkg.evGoto");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
          msg.add("aFloor", aFloor);
    }
    public String toString() {
          String s="evGoto(";      
          s += "aFloor=" + AnimInstance.animToString(aFloor) + " ";
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evGoto.java
*********************************************************************/

