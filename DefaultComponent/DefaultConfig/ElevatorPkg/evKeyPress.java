/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Administrator
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evKeyPress
//!	Generated Date	: Wed, 20, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evKeyPress.java
*********************************************************************/

package ElevatorPkg;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// ElevatorPkg/evKeyPress.java                                                                  
//----------------------------------------------------------------------------

//## package ElevatorPkg 


//## event evKeyPress(String) 
public class evKeyPress extends RiJEvent implements AnimatedEvent {
    
    public static final int evKeyPress_ElevatorPkg_id = 12823;		//## ignore 
    
    public String aLine;
    
    // Constructors
    
    public  evKeyPress() {
        lId = evKeyPress_ElevatorPkg_id;
    }
    public  evKeyPress(String p_aLine) {
        lId = evKeyPress_ElevatorPkg_id;
        aLine = p_aLine;
    }
    
    public boolean isTypeOf(long id) {
        return (evKeyPress_ElevatorPkg_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("ElevatorPkg.evKeyPress");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
          msg.add("aLine", aLine);
    }
    public String toString() {
          String s="evKeyPress(";      
          s += "aLine=" + AnimInstance.animToString(aLine) + " ";
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evKeyPress.java
*********************************************************************/

