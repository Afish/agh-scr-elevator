/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Administrator
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evOpen
//!	Generated Date	: Wed, 20, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evOpen.java
*********************************************************************/

package ElevatorPkg;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// ElevatorPkg/evOpen.java                                                                  
//----------------------------------------------------------------------------

//## package ElevatorPkg 


//## event evOpen(int,int) 
public class evOpen extends RiJEvent implements AnimatedEvent {
    
    public static final int evOpen_ElevatorPkg_id = 12816;		//## ignore 
    
    public int anId;
    public int aFloor;
    
    // Constructors
    
    public  evOpen() {
        lId = evOpen_ElevatorPkg_id;
    }
    public  evOpen(int p_anId, int p_aFloor) {
        lId = evOpen_ElevatorPkg_id;
        anId = p_anId;
        aFloor = p_aFloor;
    }
    
    public boolean isTypeOf(long id) {
        return (evOpen_ElevatorPkg_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("ElevatorPkg.evOpen");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
          msg.add("anId", anId);
          msg.add("aFloor", aFloor);
    }
    public String toString() {
          String s="evOpen(";      
          s += "anId=" + AnimInstance.animToString(anId) + " ";
          s += "aFloor=" + AnimInstance.animToString(aFloor) + " ";
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ElevatorPkg/evOpen.java
*********************************************************************/

