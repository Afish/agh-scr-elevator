/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: ElevatorGui
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Gui/ElevatorGui.java
*********************************************************************/

package Gui;

//## dependency Elevator 
import Elevator.*;
//## classInstance theBuilding 
import Elevator.Building;
//## attribute theElevator 
import Elevator.Elevator;
//## class ElevatorGui 
import Elevator.IHardware;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Gui/ElevatorGui.java                                                                  
//----------------------------------------------------------------------------

//## package Gui 


//## class ElevatorGui 
public class ElevatorGui implements RiJStateConcept, Animated, IHardware {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassElevatorGui = new AnimClass("Gui.ElevatorGui",false);
    //#]
    
    public Reactive reactive;		//## ignore 
    
    protected boolean AAdministrationMode = false;		//## attribute AAdministrationMode 
    
    protected boolean AEmergencyStopped = false;		//## attribute AEmergencyStopped 
    
    protected boolean ALocked = false;		//## attribute ALocked 
    
    protected boolean AOnFloor1 = true;		//## attribute AOnFloor1 
    
    protected boolean AOnFloor2 = false;		//## attribute AOnFloor2 
    
    protected boolean AOnFloor3 = false;		//## attribute AOnFloor3 
    
    protected boolean AOnFloor4 = false;		//## attribute AOnFloor4 
    
    protected boolean AOnFloor5 = false;		//## attribute AOnFloor5 
    
    protected boolean BAdministrationMode = false;		//## attribute BAdministrationMode 
    
    protected boolean BEmergencyStopped = false;		//## attribute BEmergencyStopped 
    
    protected boolean BLocked = false;		//## attribute BLocked 
    
    protected boolean BOnFloor1 = true;		//## attribute BOnFloor1 
    
    protected boolean BOnFloor2 = false;		//## attribute BOnFloor2 
    
    protected boolean BOnFloor3 = false;		//## attribute BOnFloor3 
    
    protected boolean BOnFloor4 = false;		//## attribute BOnFloor4 
    
    protected boolean BOnFloor5 = false;		//## attribute BOnFloor5 
    
    protected int currentFloorA = 1;		//## attribute currentFloorA 
    
    protected int currentFloorB = 1;		//## attribute currentFloorB 
    
    protected boolean floorA1Selected;		//## attribute floorA1Selected 
    
    protected boolean floorA2Selected;		//## attribute floorA2Selected 
    
    protected boolean floorA3Selected;		//## attribute floorA3Selected 
    
    protected boolean floorA4Selected;		//## attribute floorA4Selected 
    
    protected boolean floorA5Selected = false;		//## attribute floorA5Selected 
    
    protected boolean floorB1Selected;		//## attribute floorB1Selected 
    
    protected boolean floorB2Selected;		//## attribute floorB2Selected 
    
    protected boolean floorB3Selected;		//## attribute floorB3Selected 
    
    protected boolean floorB4Selected;		//## attribute floorB4Selected 
    
    protected boolean floorB5Selected;		//## attribute floorB5Selected 
    
    protected boolean goDownFromFloor2Selected;		//## attribute goDownFromFloor2Selected 
    
    protected boolean goDownFromFloor3Selected;		//## attribute goDownFromFloor3Selected 
    
    protected boolean goDownFromFloor4Selected;		//## attribute goDownFromFloor4Selected 
    
    protected boolean goDownFromFloor5Selected;		//## attribute goDownFromFloor5Selected 
    
    protected boolean goUpFromFloor1Selected;		//## attribute goUpFromFloor1Selected 
    
    protected boolean goUpFromFloor2Selected;		//## attribute goUpFromFloor2Selected 
    
    protected boolean goUpFromFloor3Selected;		//## attribute goUpFromFloor3Selected 
    
    protected boolean goUpFromFloor4Selected;		//## attribute goUpFromFloor4Selected 
    
    protected Elevator[] theElevator;		//## attribute theElevator 
    
    protected Building theBuilding;		//## classInstance theBuilding 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int unlockingB=1;
    public static final int unlockingA=2;
    public static final int normalMode=3;
    public static final int lockingB=4;
    public static final int lockingA=5;
    public static final int fastClosingB=6;
    public static final int fastClosingA=7;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## operation ElevatorGui() 
    public  ElevatorGui(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassElevatorGui.getUserClass(),
               new ArgData[] {
               });
        
        reactive = new Reactive(p_thread);
        initRelations(p_thread);
        //#[ operation ElevatorGui() 
        theElevator = new Elevator[2];
        theBuilding.configure(this);
        theBuilding.startBehavior();                        
        theElevator[0] = theBuilding.theElevator.get(0);
        theElevator[1] = theBuilding.theElevator.get(1);
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation check(int,int) 
    public void check(int anElevator, int aFloor) {
        try {
            animInstance().notifyMethodEntered("check",
               new ArgData[] {
                   new ArgData(int.class, "anElevator", AnimInstance.animToString(anElevator)),
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor))
               });
        
        //#[ operation check(int,int) 
        switch ( anElevator ) {
        case 0:
        	switch ( aFloor ) {
        		case 0: floorA1Selected = true;	break;
        		case 1: floorA2Selected = true;	break;
        		case 2: floorA3Selected = true;	break;
        		case 3: floorA4Selected = true;	break;
        		case 4: floorA5Selected = true;	break;
        	}
        	break;
        case 1:
        	switch ( aFloor ) {
        		case 0: floorB1Selected = true;	break;
        		case 1: floorB2Selected = true;	break;
        		case 2: floorB3Selected = true;	break;
        		case 3: floorB4Selected = true;	break;
        		case 4: floorB5Selected = true;	break;
        	}
        	break;
        }
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation close(int,int) 
    public void close(int anElevator, int aFloor) {
        try {
            animInstance().notifyMethodEntered("close",
               new ArgData[] {
                   new ArgData(int.class, "anElevator", AnimInstance.animToString(anElevator)),
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor))
               });
        
        //#[ operation close(int,int) 
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation closeTheDoorNowA() 
    public void closeTheDoorNowA() {
        try {
            animInstance().notifyMethodEntered("closeTheDoorNowA",
               new ArgData[] {
               });
        
        //#[ operation closeTheDoorNowA() 
        	performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[0].closeTheDoorNow();
        	}
        	});
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation closeTheDoorNowB() 
    public void closeTheDoorNowB() {
        try {
            animInstance().notifyMethodEntered("closeTheDoorNowB",
               new ArgData[] {
               });
        
        //#[ operation closeTheDoorNowB() 
         	performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[1].closeTheDoorNow();
        	}
        	});
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation closing(int,int) 
    public void closing(int anElevator, int aFloor) {
        try {
            animInstance().notifyMethodEntered("closing",
               new ArgData[] {
                   new ArgData(int.class, "anElevator", AnimInstance.animToString(anElevator)),
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor))
               });
        
        //#[ operation closing(int,int) 
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation lockA() 
    public void lockA() {
        try {
            animInstance().notifyMethodEntered("lockA",
               new ArgData[] {
               });
        
        //#[ operation lockA() 
        if(theElevator[0].lock())
        {
        	performInOtherThread(new Runnable(){
        		public void run(){         
        			ALocked = true;
        		}
        	});
        } 
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation lockB() 
    public void lockB() {
        try {
            animInstance().notifyMethodEntered("lockB",
               new ArgData[] {
               });
        
        //#[ operation lockB() 
        if(theElevator[1].lock())
        {
        	performInOtherThread(new Runnable(){
        		public void run(){         
        			BLocked = true;
        		}
        	});
        } 
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation moving(int,int) 
    public void moving(int anElevator, int aFloor) {
        try {
            animInstance().notifyMethodEntered("moving",
               new ArgData[] {
                   new ArgData(int.class, "anElevator", AnimInstance.animToString(anElevator)),
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor))
               });
        
        //#[ operation moving(int,int) 
        switch ( anElevator ) {
        case 0:         
        	AOnFloor1 = AOnFloor2 = AOnFloor3 = AOnFloor4 = AOnFloor5 = false;
        	switch ( aFloor ) {
        		case 0: currentFloorA = 1; AOnFloor1 = true; break;
        		case 1: currentFloorA = 2; AOnFloor2 = true; break;
        		case 2: currentFloorA = 3; AOnFloor3 = true; break;
        		case 3: currentFloorA = 4; AOnFloor4 = true; break;
        		case 4: currentFloorA = 5; AOnFloor5 = true; break;
        	}
        	break;
        case 1:         
        	BOnFloor1 = BOnFloor2 = BOnFloor3 = BOnFloor4 = BOnFloor5 = false;
        	switch ( aFloor ) {
        		case 0: currentFloorB = 1; BOnFloor1 = true; break;
        		case 1: currentFloorB = 2; BOnFloor2 = true; break;
        		case 2: currentFloorB = 3; BOnFloor3 = true; break;
        		case 3: currentFloorB = 4; BOnFloor4 = true; break;
        		case 4: currentFloorB = 5; BOnFloor5 = true; break;
        	}
        	break;
        }
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation open(int,int) 
    public void open(int anElevator, int aFloor) {
        try {
            animInstance().notifyMethodEntered("open",
               new ArgData[] {
                   new ArgData(int.class, "anElevator", AnimInstance.animToString(anElevator)),
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor))
               });
        
        //#[ operation open(int,int) 
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation opening(int,int) 
    public void opening(int anElevator, int aFloor) {
        try {
            animInstance().notifyMethodEntered("opening",
               new ArgData[] {
                   new ArgData(int.class, "anElevator", AnimInstance.animToString(anElevator)),
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor))
               });
        
        //#[ operation opening(int,int) 
        uncheck ( anElevator, aFloor );
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param callback
     * @param callback
    */
    //## operation performInOtherThread(Runnable) 
    public void performInOtherThread(Runnable callback) {
        try {
            animInstance().notifyMethodEntered("performInOtherThread",
               new ArgData[] {
                   new ArgData(Runnable.class, "callback", AnimInstance.animToString(callback))
               });
        
        //#[ operation performInOtherThread(Runnable) 
        try{                           
        	Thread thread = new Thread(callback);
        	thread.start();
        }catch(Exception e){
        	System.out.println("Exception while performing in other thread: " + e.getMessage());
        }
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param active
    */
    //## operation setAAdministrationMode(boolean) 
    public void setAAdministrationMode(boolean active) {
        try {
            animInstance().notifyMethodEntered("setAAdministrationMode",
               new ArgData[] {
                   new ArgData(boolean.class, "active", AnimInstance.animToString(active))
               });
        
        //#[ operation setAAdministrationMode(boolean) 
        AAdministrationMode = active;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param active
    */
    //## operation setAEmergencyStopped(boolean) 
    public void setAEmergencyStopped(boolean active) {
        try {
            animInstance().notifyMethodEntered("setAEmergencyStopped",
               new ArgData[] {
                   new ArgData(boolean.class, "active", AnimInstance.animToString(active))
               });
        
        //#[ operation setAEmergencyStopped(boolean) 
        AEmergencyStopped = active;
        if(active)
        {                         
        	performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[0].gen(new evEmergencyStop());
        	}
        	});
        }
        else
        {                    
        	performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[0].gen(new evEmergencyCancel());
        	}
        	});
        }
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param active
    */
    //## operation setBAdministrationMode(boolean) 
    public void setBAdministrationMode(boolean active) {
        try {
            animInstance().notifyMethodEntered("setBAdministrationMode",
               new ArgData[] {
                   new ArgData(boolean.class, "active", AnimInstance.animToString(active))
               });
        
        //#[ operation setBAdministrationMode(boolean) 
        BAdministrationMode = active;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param active
    */
    //## operation setBEmergencyStopped(boolean) 
    public void setBEmergencyStopped(boolean active) {
        try {
            animInstance().notifyMethodEntered("setBEmergencyStopped",
               new ArgData[] {
                   new ArgData(boolean.class, "active", AnimInstance.animToString(active))
               });
        
        //#[ operation setBEmergencyStopped(boolean) 
             BEmergencyStopped = active;
        if(active)
        {                         
        	performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[1].gen(new evEmergencyStop());
        	}
        	});
        }
        else
        {                    
        	performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[1].gen(new evEmergencyCancel());
        	}
        	});
        }
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setFloorA1Selected(boolean) 
    public void setFloorA1Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setFloorA1Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setFloorA1Selected(boolean) 
        check(0, 0);                               
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[0].gen(new evGoto(0) );
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setFloorA2Selected(boolean) 
    public void setFloorA2Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setFloorA2Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setFloorA2Selected(boolean) 
        check(0, 1);   
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[0].gen(new evGoto(1) );
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setFloorA3Selected(boolean) 
    public void setFloorA3Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setFloorA3Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setFloorA3Selected(boolean) 
        check(0, 2);   
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[0].gen(new evGoto(2) );
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setFloorA4Selected(boolean) 
    public void setFloorA4Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setFloorA4Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setFloorA4Selected(boolean) 
        check(0, 3);                
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[0].gen(new evGoto(3) );
        	}
        });
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setFloorA5Selected(boolean) 
    public void setFloorA5Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setFloorA5Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setFloorA5Selected(boolean) 
        
        if(!AAdministrationMode && selected == false){     
        	floorA5Selected = false;
        	return;
        }
        if(!AAdministrationMode)
        {
        	performInOtherThread(new Runnable(){
        		public void run(){         
        			floorA5Selected = true;
        			setFloorA5Selected(false);
        		}
        	});    
        	return;
        }
        check(0, 4);  
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[0].gen(new evGoto(4) );
        	}
        });
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setFloorB1Selected(boolean) 
    public void setFloorB1Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setFloorB1Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setFloorB1Selected(boolean) 
        check(1, 0);      
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[1].gen(new evGoto(0) );
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setFloorB2Selected(boolean) 
    public void setFloorB2Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setFloorB2Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setFloorB2Selected(boolean) 
        check(1, 1);     
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[1].gen(new evGoto(1) );
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setFloorB3Selected(boolean) 
    public void setFloorB3Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setFloorB3Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setFloorB3Selected(boolean) 
        check(1, 2);   
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[1].gen(new evGoto(2) );
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setFloorB4Selected(boolean) 
    public void setFloorB4Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setFloorB4Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setFloorB4Selected(boolean) 
        check(1, 3);       
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[1].gen(new evGoto(3) );
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setFloorB5Selected(boolean) 
    public void setFloorB5Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setFloorB5Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setFloorB5Selected(boolean) 
        
        if(!BAdministrationMode && selected == false){     
        	floorB5Selected = false;
        	return;
        }
        if(!BAdministrationMode)
        {
        	performInOtherThread(new Runnable(){
        		public void run(){         
        			floorB5Selected = true;
        			setFloorB5Selected(false);
        		}
        	});    
        	return;
        }
        check(1, 4);  
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theElevator[1].gen(new evGoto(4) );
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setGoDownFromFloor2Selected(boolean) 
    public void setGoDownFromFloor2Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setGoDownFromFloor2Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setGoDownFromFloor2Selected(boolean) 
        goDownFromFloor2Selected = selected;    
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theBuilding.gen(new evCall(1, Direction.DOWN ));
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setGoDownFromFloor3Selected(boolean) 
    public void setGoDownFromFloor3Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setGoDownFromFloor3Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setGoDownFromFloor3Selected(boolean) 
        goDownFromFloor3Selected = selected;     
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theBuilding.gen(new evCall(2, Direction.DOWN ));
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setGoDownFromFloor4Selected(boolean) 
    public void setGoDownFromFloor4Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setGoDownFromFloor4Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setGoDownFromFloor4Selected(boolean) 
        goDownFromFloor4Selected = selected;    
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theBuilding.gen(new evCall(3, Direction.DOWN ));
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setGoDownFromFloor5Selected(boolean) 
    public void setGoDownFromFloor5Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setGoDownFromFloor5Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setGoDownFromFloor5Selected(boolean) 
        goDownFromFloor5Selected = selected;    
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theBuilding.gen(new evCall(4, Direction.DOWN ));
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setGoUpFromFloor1Selected(boolean) 
    public void setGoUpFromFloor1Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setGoUpFromFloor1Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setGoUpFromFloor1Selected(boolean) 
        goUpFromFloor1Selected = selected;      
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theBuilding.gen(new evCall(0, Direction.UP ));
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setGoUpFromFloor2Selected(boolean) 
    public void setGoUpFromFloor2Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setGoUpFromFloor2Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setGoUpFromFloor2Selected(boolean) 
        goUpFromFloor2Selected = selected;        
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theBuilding.gen(new evCall(1, Direction.UP ));
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setGoUpFromFloor3Selected(boolean) 
    public void setGoUpFromFloor3Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setGoUpFromFloor3Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setGoUpFromFloor3Selected(boolean) 
        goUpFromFloor3Selected = selected;    
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theBuilding.gen(new evCall(2, Direction.UP ));
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param selected
    */
    //## operation setGoUpFromFloor4Selected(boolean) 
    public void setGoUpFromFloor4Selected(boolean selected) {
        try {
            animInstance().notifyMethodEntered("setGoUpFromFloor4Selected",
               new ArgData[] {
                   new ArgData(boolean.class, "selected", AnimInstance.animToString(selected))
               });
        
        //#[ operation setGoUpFromFloor4Selected(boolean) 
        goUpFromFloor4Selected = selected;  
        performInOtherThread(new Runnable(){
        	public void run(){                
        		theBuilding.gen(new evCall(3, Direction.UP ));
        	}
        });
        
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    /**
     * @param anElevator
     * @param aFloor
    */
    //## operation uncheck(int,int) 
    public void uncheck(int anElevator, int aFloor) {
        try {
            animInstance().notifyMethodEntered("uncheck",
               new ArgData[] {
                   new ArgData(int.class, "anElevator", AnimInstance.animToString(anElevator)),
                   new ArgData(int.class, "aFloor", AnimInstance.animToString(aFloor))
               });
        
        //#[ operation uncheck(int,int) 
        switch ( aFloor ) {
        	case 0: goUpFromFloor1Selected = false; break;
        	case 1: goUpFromFloor2Selected = false; goDownFromFloor2Selected = false; break;
        	case 2: goUpFromFloor3Selected = false; goDownFromFloor3Selected = false; break;
        	case 3: goUpFromFloor4Selected = false; goDownFromFloor4Selected = false; break;
        	case 4: goDownFromFloor5Selected = false; break;
        }         
        
            
        switch ( anElevator ) {
        case 0:     
        	switch ( aFloor ) {
        		case 0: floorA1Selected = false; break;
        		case 1: floorA2Selected = false; break;
        		case 2: floorA3Selected = false; break;
        		case 3: floorA4Selected = false; break;
        		case 4: floorA5Selected = false; break;
        	}      
        	break;
        case 1:
        	switch ( aFloor ) {
        		case 0: floorB1Selected = false; break;
        		case 1: floorB2Selected = false; break;
        		case 2: floorB3Selected = false; break;
        		case 3: floorB4Selected = false; break;
        		case 4: floorB5Selected = false; break;
        	}      
        	break;
        }
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation unlockA() 
    public void unlockA() {
        try {
            animInstance().notifyMethodEntered("unlockA",
               new ArgData[] {
               });
        
        //#[ operation unlockA() 
        performInOtherThread(new Runnable(){
        public void run(){                
        	theElevator[0].unlock();
        }
        }); 
        ALocked = false;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation unlockB() 
    public void unlockB() {
        try {
            animInstance().notifyMethodEntered("unlockB",
               new ArgData[] {
               });
        
        //#[ operation unlockB() 
        performInOtherThread(new Runnable(){
        public void run(){                
        	theElevator[1].unlock();
        }
        });
        BLocked = false;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public boolean getAAdministrationMode() {
        return AAdministrationMode;
    }
    
    //## auto_generated 
    public boolean getAEmergencyStopped() {
        return AEmergencyStopped;
    }
    
    //## auto_generated 
    public boolean getALocked() {
        return ALocked;
    }
    
    //## auto_generated 
    public void setALocked(boolean p_ALocked) {
        try {
        ALocked = p_ALocked;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public boolean getAOnFloor1() {
        return AOnFloor1;
    }
    
    //## auto_generated 
    public void setAOnFloor1(boolean p_AOnFloor1) {
        try {
        AOnFloor1 = p_AOnFloor1;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public boolean getAOnFloor2() {
        return AOnFloor2;
    }
    
    //## auto_generated 
    public void setAOnFloor2(boolean p_AOnFloor2) {
        try {
        AOnFloor2 = p_AOnFloor2;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public boolean getAOnFloor3() {
        return AOnFloor3;
    }
    
    //## auto_generated 
    public void setAOnFloor3(boolean p_AOnFloor3) {
        try {
        AOnFloor3 = p_AOnFloor3;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public boolean getAOnFloor4() {
        return AOnFloor4;
    }
    
    //## auto_generated 
    public void setAOnFloor4(boolean p_AOnFloor4) {
        try {
        AOnFloor4 = p_AOnFloor4;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public boolean getAOnFloor5() {
        return AOnFloor5;
    }
    
    //## auto_generated 
    public void setAOnFloor5(boolean p_AOnFloor5) {
        try {
        AOnFloor5 = p_AOnFloor5;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public boolean getBAdministrationMode() {
        return BAdministrationMode;
    }
    
    //## auto_generated 
    public boolean getBEmergencyStopped() {
        return BEmergencyStopped;
    }
    
    //## auto_generated 
    public boolean getBLocked() {
        return BLocked;
    }
    
    //## auto_generated 
    public void setBLocked(boolean p_BLocked) {
        try {
        BLocked = p_BLocked;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public boolean getBOnFloor1() {
        return BOnFloor1;
    }
    
    //## auto_generated 
    public void setBOnFloor1(boolean p_BOnFloor1) {
        try {
        BOnFloor1 = p_BOnFloor1;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public boolean getBOnFloor2() {
        return BOnFloor2;
    }
    
    //## auto_generated 
    public void setBOnFloor2(boolean p_BOnFloor2) {
        try {
        BOnFloor2 = p_BOnFloor2;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public boolean getBOnFloor3() {
        return BOnFloor3;
    }
    
    //## auto_generated 
    public void setBOnFloor3(boolean p_BOnFloor3) {
        try {
        BOnFloor3 = p_BOnFloor3;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public boolean getBOnFloor4() {
        return BOnFloor4;
    }
    
    //## auto_generated 
    public void setBOnFloor4(boolean p_BOnFloor4) {
        try {
        BOnFloor4 = p_BOnFloor4;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public boolean getBOnFloor5() {
        return BOnFloor5;
    }
    
    //## auto_generated 
    public void setBOnFloor5(boolean p_BOnFloor5) {
        try {
        BOnFloor5 = p_BOnFloor5;
        }
        finally {
            animInstance().notifyUpdatedAttr();
        }
    }
    
    //## auto_generated 
    public int getCurrentFloorA() {
        return currentFloorA;
    }
    
    //## auto_generated 
    public void setCurrentFloorA(int p_currentFloorA) {
        currentFloorA = p_currentFloorA;
    }
    
    //## auto_generated 
    public int getCurrentFloorB() {
        return currentFloorB;
    }
    
    //## auto_generated 
    public void setCurrentFloorB(int p_currentFloorB) {
        currentFloorB = p_currentFloorB;
    }
    
    //## auto_generated 
    public boolean getFloorA1Selected() {
        return floorA1Selected;
    }
    
    //## auto_generated 
    public boolean getFloorA2Selected() {
        return floorA2Selected;
    }
    
    //## auto_generated 
    public boolean getFloorA3Selected() {
        return floorA3Selected;
    }
    
    //## auto_generated 
    public boolean getFloorA4Selected() {
        return floorA4Selected;
    }
    
    //## auto_generated 
    public boolean getFloorA5Selected() {
        return floorA5Selected;
    }
    
    //## auto_generated 
    public boolean getFloorB1Selected() {
        return floorB1Selected;
    }
    
    //## auto_generated 
    public boolean getFloorB2Selected() {
        return floorB2Selected;
    }
    
    //## auto_generated 
    public boolean getFloorB3Selected() {
        return floorB3Selected;
    }
    
    //## auto_generated 
    public boolean getFloorB4Selected() {
        return floorB4Selected;
    }
    
    //## auto_generated 
    public boolean getFloorB5Selected() {
        return floorB5Selected;
    }
    
    //## auto_generated 
    public boolean getGoDownFromFloor2Selected() {
        return goDownFromFloor2Selected;
    }
    
    //## auto_generated 
    public boolean getGoDownFromFloor3Selected() {
        return goDownFromFloor3Selected;
    }
    
    //## auto_generated 
    public boolean getGoDownFromFloor4Selected() {
        return goDownFromFloor4Selected;
    }
    
    //## auto_generated 
    public boolean getGoDownFromFloor5Selected() {
        return goDownFromFloor5Selected;
    }
    
    //## auto_generated 
    public boolean getGoUpFromFloor1Selected() {
        return goUpFromFloor1Selected;
    }
    
    //## auto_generated 
    public boolean getGoUpFromFloor2Selected() {
        return goUpFromFloor2Selected;
    }
    
    //## auto_generated 
    public boolean getGoUpFromFloor3Selected() {
        return goUpFromFloor3Selected;
    }
    
    //## auto_generated 
    public boolean getGoUpFromFloor4Selected() {
        return goUpFromFloor4Selected;
    }
    
    //## auto_generated 
    public Elevator getTheElevator(int i1) {
        return theElevator[i1];
    }
    
    //## auto_generated 
    public void setTheElevator(int i1, Elevator p_theElevator) {
        theElevator[i1] = p_theElevator;
    }
    
    //## auto_generated 
    public Building getTheBuilding() {
        return theBuilding;
    }
    
    //## auto_generated 
    public void __setTheBuilding(Building p_Building) {
        theBuilding = p_Building;
        if(p_Building != null)
            {
                animInstance().notifyRelationAdded("theBuilding", p_Building);
            }
        else
            {
                animInstance().notifyRelationCleared("theBuilding");
            }
    }
    
    //## auto_generated 
    public void _setTheBuilding(Building p_Building) {
        if(theBuilding != null)
            {
                theBuilding.__setItsGui(null);
            }
        __setTheBuilding(p_Building);
    }
    
    //## auto_generated 
    public Building newTheBuilding(RiJThread p_thread) {
        theBuilding = new Building(p_thread);
        theBuilding._setItsGui(this);
        animInstance().notifyRelationAdded("theBuilding", theBuilding);
        return theBuilding;
    }
    
    //## auto_generated 
    public void deleteTheBuilding() {
        theBuilding.__setItsGui(null);
        animInstance().notifyRelationRemoved("theBuilding", theBuilding);
        theBuilding=null;
    }
    
    //## auto_generated 
    protected void initRelations(RiJThread p_thread) {
        theBuilding = newTheBuilding(p_thread);
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = true;
        done &= theBuilding.startBehavior();
        done &= reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            switch (rootState_subState) {
                case normalMode:
                {
                    normalMode_add(animStates);
                }
                break;
                case fastClosingA:
                {
                    fastClosingA_add(animStates);
                }
                break;
                case fastClosingB:
                {
                    fastClosingB_add(animStates);
                }
                break;
                case lockingA:
                {
                    lockingA_add(animStates);
                }
                break;
                case unlockingA:
                {
                    unlockingA_add(animStates);
                }
                break;
                case lockingB:
                {
                    lockingB_add(animStates);
                }
                break;
                case unlockingB:
                {
                    unlockingB_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case normalMode:
                {
                    res = normalMode_takeEvent(id);
                }
                break;
                case fastClosingA:
                {
                    res = fastClosingA_takeEvent(id);
                }
                break;
                case fastClosingB:
                {
                    res = fastClosingB_takeEvent(id);
                }
                break;
                case lockingA:
                {
                    res = lockingA_takeEvent(id);
                }
                break;
                case unlockingA:
                {
                    res = unlockingA_takeEvent(id);
                }
                break;
                case lockingB:
                {
                    res = lockingB_takeEvent(id);
                }
                break;
                case unlockingB:
                {
                    res = unlockingB_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void unlockingB_add(AnimStates animStates) {
            animStates.add("ROOT.unlockingB");
        }
        
        //## statechart_method 
        public void unlockingA_add(AnimStates animStates) {
            animStates.add("ROOT.unlockingA");
        }
        
        //## statechart_method 
        public void normalMode_add(AnimStates animStates) {
            animStates.add("ROOT.normalMode");
        }
        
        //## statechart_method 
        public void lockingB_add(AnimStates animStates) {
            animStates.add("ROOT.lockingB");
        }
        
        //## statechart_method 
        public void lockingA_add(AnimStates animStates) {
            animStates.add("ROOT.lockingA");
        }
        
        //## statechart_method 
        public void fastClosingB_add(AnimStates animStates) {
            animStates.add("ROOT.fastClosingB");
        }
        
        //## statechart_method 
        public void fastClosingA_add(AnimStates animStates) {
            animStates.add("ROOT.fastClosingA");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
        }
        
        //## statechart_method 
        public void fastClosingA_enter() {
            animInstance().notifyStateEntered("ROOT.fastClosingA");
            pushNullConfig();
            rootState_subState = fastClosingA;
            rootState_active = fastClosingA;
            fastClosingAEnter();
        }
        
        //## statechart_method 
        public void fastClosingBExit() {
        }
        
        //## statechart_method 
        public void lockingB_entDef() {
            lockingB_enter();
        }
        
        //## statechart_method 
        public void unlockingB_exit() {
            popNullConfig();
            unlockingBExit();
            animInstance().notifyStateExited("ROOT.unlockingB");
        }
        
        //## statechart_method 
        public void unlockingB_enter() {
            animInstance().notifyStateEntered("ROOT.unlockingB");
            pushNullConfig();
            rootState_subState = unlockingB;
            rootState_active = unlockingB;
            unlockingBEnter();
        }
        
        //## statechart_method 
        public void fastClosingB_enter() {
            animInstance().notifyStateEntered("ROOT.fastClosingB");
            pushNullConfig();
            rootState_subState = fastClosingB;
            rootState_active = fastClosingB;
            fastClosingBEnter();
        }
        
        //## statechart_method 
        public void fastClosingBEnter() {
        }
        
        //## statechart_method 
        public int normalModeTakeevUnlockA() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("7");
            normalMode_exit();
            unlockingA_entDef();
            animInstance().notifyTransitionEnded("7");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void unlockingA_exit() {
            popNullConfig();
            unlockingAExit();
            animInstance().notifyStateExited("ROOT.unlockingA");
        }
        
        //## statechart_method 
        public int fastClosingATakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("2");
            fastClosingA_exit();
            //#[ transition 2 
            closeTheDoorNowA();
            //#]
            normalMode_entDef();
            animInstance().notifyTransitionEnded("2");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void fastClosingAEnter() {
        }
        
        //## statechart_method 
        public int normalModeTakeevUnlockB() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("11");
            normalMode_exit();
            unlockingB_entDef();
            animInstance().notifyTransitionEnded("11");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int fastClosingA_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = fastClosingATakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int lockingA_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = lockingATakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void fastClosingA_entDef() {
            fastClosingA_enter();
        }
        
        //## statechart_method 
        public void fastClosingB_exit() {
            popNullConfig();
            fastClosingBExit();
            animInstance().notifyStateExited("ROOT.fastClosingB");
        }
        
        //## statechart_method 
        public void lockingAExit() {
        }
        
        //## statechart_method 
        public void normalModeExit() {
        }
        
        //## statechart_method 
        public void unlockingB_entDef() {
            unlockingB_enter();
        }
        
        //## statechart_method 
        public void fastClosingA_exit() {
            popNullConfig();
            fastClosingAExit();
            animInstance().notifyStateExited("ROOT.fastClosingA");
        }
        
        //## statechart_method 
        public void lockingBExit() {
        }
        
        //## statechart_method 
        public void lockingBEnter() {
        }
        
        //## statechart_method 
        public void lockingAEnter() {
        }
        
        //## statechart_method 
        public void unlockingAExit() {
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public int normalMode_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evCloseNowA.evCloseNowA_Gui_id))
                {
                    res = normalModeTakeevCloseNowA();
                }
            else if(event.isTypeOf(evCloseNowB.evCloseNowB_Gui_id))
                {
                    res = normalModeTakeevCloseNowB();
                }
            else if(event.isTypeOf(evLockA.evLockA_Gui_id))
                {
                    res = normalModeTakeevLockA();
                }
            else if(event.isTypeOf(evLockB.evLockB_Gui_id))
                {
                    res = normalModeTakeevLockB();
                }
            else if(event.isTypeOf(evUnlockA.evUnlockA_Gui_id))
                {
                    res = normalModeTakeevUnlockA();
                }
            else if(event.isTypeOf(evUnlockB.evUnlockB_Gui_id))
                {
                    res = normalModeTakeevUnlockB();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int normalModeTakeevCloseNowA() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("1");
            normalMode_exit();
            fastClosingA_entDef();
            animInstance().notifyTransitionEnded("1");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void unlockingBExit() {
        }
        
        //## statechart_method 
        public void lockingB_exit() {
            popNullConfig();
            lockingBExit();
            animInstance().notifyStateExited("ROOT.lockingB");
        }
        
        //## statechart_method 
        public int normalModeTakeevLockA() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("5");
            normalMode_exit();
            lockingA_entDef();
            animInstance().notifyTransitionEnded("5");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int normalModeTakeevCloseNowB() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("3");
            normalMode_exit();
            fastClosingB_entDef();
            animInstance().notifyTransitionEnded("3");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int unlockingB_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = unlockingBTakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void lockingA_exit() {
            popNullConfig();
            lockingAExit();
            animInstance().notifyStateExited("ROOT.lockingA");
        }
        
        //## statechart_method 
        public int normalModeTakeevLockB() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("9");
            normalMode_exit();
            lockingB_entDef();
            animInstance().notifyTransitionEnded("9");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void normalMode_enter() {
            animInstance().notifyStateEntered("ROOT.normalMode");
            rootState_subState = normalMode;
            rootState_active = normalMode;
            normalModeEnter();
        }
        
        //## statechart_method 
        public void fastClosingB_entDef() {
            fastClosingB_enter();
        }
        
        //## statechart_method 
        public void lockingA_enter() {
            animInstance().notifyStateEntered("ROOT.lockingA");
            pushNullConfig();
            rootState_subState = lockingA;
            rootState_active = lockingA;
            lockingAEnter();
        }
        
        //## statechart_method 
        public void lockingA_entDef() {
            lockingA_enter();
        }
        
        //## statechart_method 
        public void normalModeEnter() {
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("0");
            normalMode_entDef();
            animInstance().notifyTransitionEnded("0");
        }
        
        //## statechart_method 
        public int lockingBTakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("10");
            lockingB_exit();
            //#[ transition 10 
            lockB();
            //#]
            normalMode_entDef();
            animInstance().notifyTransitionEnded("10");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void lockingB_enter() {
            animInstance().notifyStateEntered("ROOT.lockingB");
            pushNullConfig();
            rootState_subState = lockingB;
            rootState_active = lockingB;
            lockingBEnter();
        }
        
        //## statechart_method 
        public void normalMode_entDef() {
            normalMode_enter();
        }
        
        //## statechart_method 
        public int unlockingA_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = unlockingATakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int unlockingBTakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("12");
            unlockingB_exit();
            //#[ transition 12 
            unlockB();
            //#]
            normalMode_entDef();
            animInstance().notifyTransitionEnded("12");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int lockingB_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = lockingBTakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void normalMode_exit() {
            normalModeExit();
            animInstance().notifyStateExited("ROOT.normalMode");
        }
        
        //## statechart_method 
        public void unlockingBEnter() {
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public int fastClosingB_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = fastClosingBTakeNull();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int fastClosingBTakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("4");
            fastClosingB_exit();
            //#[ transition 4 
            closeTheDoorNowB();
            //#]
            normalMode_entDef();
            animInstance().notifyTransitionEnded("4");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int lockingATakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("6");
            lockingA_exit();
            //#[ transition 6 
            lockA();
            //#]
            normalMode_entDef();
            animInstance().notifyTransitionEnded("6");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void unlockingAEnter() {
        }
        
        //## statechart_method 
        public void unlockingA_entDef() {
            unlockingA_enter();
        }
        
        //## statechart_method 
        public void fastClosingAExit() {
        }
        
        //## statechart_method 
        public int unlockingATakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("8");
            unlockingA_exit();
            //#[ transition 8 
            unlockA();
            //#]
            normalMode_entDef();
            animInstance().notifyTransitionEnded("8");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void unlockingA_enter() {
            animInstance().notifyStateEntered("ROOT.unlockingA");
            pushNullConfig();
            rootState_subState = unlockingA;
            rootState_active = unlockingA;
            unlockingAEnter();
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return ElevatorGui.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassElevatorGui; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("theElevator", theElevator);
        msg.add("currentFloorA", currentFloorA);
        msg.add("currentFloorB", currentFloorB);
        msg.add("floorA1Selected", floorA1Selected);
        msg.add("floorA2Selected", floorA2Selected);
        msg.add("floorA3Selected", floorA3Selected);
        msg.add("floorA4Selected", floorA4Selected);
        msg.add("floorA5Selected", floorA5Selected);
        msg.add("floorB1Selected", floorB1Selected);
        msg.add("floorB2Selected", floorB2Selected);
        msg.add("floorB3Selected", floorB3Selected);
        msg.add("floorB4Selected", floorB4Selected);
        msg.add("floorB5Selected", floorB5Selected);
        msg.add("goDownFromFloor2Selected", goDownFromFloor2Selected);
        msg.add("goDownFromFloor3Selected", goDownFromFloor3Selected);
        msg.add("goDownFromFloor4Selected", goDownFromFloor4Selected);
        msg.add("goDownFromFloor5Selected", goDownFromFloor5Selected);
        msg.add("goUpFromFloor1Selected", goUpFromFloor1Selected);
        msg.add("goUpFromFloor2Selected", goUpFromFloor2Selected);
        msg.add("goUpFromFloor3Selected", goUpFromFloor3Selected);
        msg.add("goUpFromFloor4Selected", goUpFromFloor4Selected);
        msg.add("AOnFloor1", AOnFloor1);
        msg.add("AOnFloor2", AOnFloor2);
        msg.add("AOnFloor3", AOnFloor3);
        msg.add("AOnFloor4", AOnFloor4);
        msg.add("AOnFloor5", AOnFloor5);
        msg.add("BOnFloor1", BOnFloor1);
        msg.add("BOnFloor2", BOnFloor2);
        msg.add("BOnFloor3", BOnFloor3);
        msg.add("BOnFloor4", BOnFloor4);
        msg.add("BOnFloor5", BOnFloor5);
        msg.add("AEmergencyStopped", AEmergencyStopped);
        msg.add("BEmergencyStopped", BEmergencyStopped);
        msg.add("AAdministrationMode", AAdministrationMode);
        msg.add("BAdministrationMode", BAdministrationMode);
        msg.add("ALocked", ALocked);
        msg.add("BLocked", BLocked);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("theBuilding", true, true, theBuilding);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(ElevatorGui.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            ElevatorGui.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            ElevatorGui.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Gui/ElevatorGui.java
*********************************************************************/

