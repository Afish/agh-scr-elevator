/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: closeTheDoorNow
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Gui/closeTheDoorNow.java
*********************************************************************/

package Gui;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Gui/closeTheDoorNow.java                                                                  
//----------------------------------------------------------------------------

//## package Gui 


//## event closeTheDoorNow() 
public class closeTheDoorNow extends RiJEvent implements AnimatedEvent {
    
    public static final int closeTheDoorNow_Gui_id = 27817;		//## ignore 
    
    
    // Constructors
    
    public  closeTheDoorNow() {
        lId = closeTheDoorNow_Gui_id;
    }
    
    public boolean isTypeOf(long id) {
        return (closeTheDoorNow_Gui_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Gui.closeTheDoorNow");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="closeTheDoorNow(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Gui/closeTheDoorNow.java
*********************************************************************/

