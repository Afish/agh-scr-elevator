/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: closeTheDoorNowA
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Gui/closeTheDoorNowA.java
*********************************************************************/

package Gui;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Gui/closeTheDoorNowA.java                                                                  
//----------------------------------------------------------------------------

//## package Gui 


//## event closeTheDoorNowA() 
public class closeTheDoorNowA extends RiJEvent implements AnimatedEvent {
    
    public static final int closeTheDoorNowA_Gui_id = 27818;		//## ignore 
    
    
    // Constructors
    
    public  closeTheDoorNowA() {
        lId = closeTheDoorNowA_Gui_id;
    }
    
    public boolean isTypeOf(long id) {
        return (closeTheDoorNowA_Gui_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Gui.closeTheDoorNowA");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="closeTheDoorNowA(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Gui/closeTheDoorNowA.java
*********************************************************************/

