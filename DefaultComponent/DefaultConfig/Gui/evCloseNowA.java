/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evCloseNowA
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Gui/evCloseNowA.java
*********************************************************************/

package Gui;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Gui/evCloseNowA.java                                                                  
//----------------------------------------------------------------------------

//## package Gui 


//## event evCloseNowA() 
public class evCloseNowA extends RiJEvent implements AnimatedEvent {
    
    public static final int evCloseNowA_Gui_id = 27816;		//## ignore 
    
    
    // Constructors
    
    public  evCloseNowA() {
        lId = evCloseNowA_Gui_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evCloseNowA_Gui_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Gui.evCloseNowA");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="evCloseNowA(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Gui/evCloseNowA.java
*********************************************************************/

