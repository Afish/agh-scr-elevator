/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: evUnlockA
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/Gui/evUnlockA.java
*********************************************************************/

package Gui;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Gui/evUnlockA.java                                                                  
//----------------------------------------------------------------------------

//## package Gui 


//## event evUnlockA() 
public class evUnlockA extends RiJEvent implements AnimatedEvent {
    
    public static final int evUnlockA_Gui_id = 27821;		//## ignore 
    
    
    // Constructors
    
    public  evUnlockA() {
        lId = evUnlockA_Gui_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evUnlockA_Gui_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Gui.evUnlockA");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="evUnlockA(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Gui/evUnlockA.java
*********************************************************************/

