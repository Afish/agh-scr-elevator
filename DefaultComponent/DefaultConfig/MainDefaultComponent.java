/*********************************************************************
	Rhapsody	: 8.0.3
	Login		: Dawid
	Component	: DefaultComponent
	Configuration 	: DefaultConfig
	Model Element	: DefaultConfig
//!	Generated Date	: Sun, 24, Nov 2013 
	File Path	: DefaultComponent/DefaultConfig/MainDefaultComponent.java
*********************************************************************/


//## auto_generated
import Gui.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.*;

//----------------------------------------------------------------------------
// MainDefaultComponent.java                                                                  
//----------------------------------------------------------------------------


//## ignore 
public class MainDefaultComponent {
    
    //#[ ignore
    // link with events in order to register them in the animation browser
    static {
      // Setting Animation Default Port 
      AnimTcpIpConnection.setDefaultPort(6423);
      // Registering Events 
      try {
        
            Class.forName("Elevator.evAtFloor");
            Class.forName("Elevator.evCall");
            Class.forName("Elevator.evChangeDirection");
            Class.forName("Elevator.evClosed");
            Class.forName("Elevator.evCloseNow");
            Class.forName("Elevator.evEmergencyCancel");
            Class.forName("Elevator.evEmergencyStop");
            Class.forName("Elevator.evGoto");
            Class.forName("Elevator.evKeyPress");
            Class.forName("Elevator.evLock");
            Class.forName("Elevator.evMove");
            Class.forName("Elevator.evOpen");
            Class.forName("Elevator.evUnlock");
            Class.forName("Gui.closeTheDoorNow");
            Class.forName("Gui.closeTheDoorNowA");
            Class.forName("Gui.evCloseNowA");
            Class.forName("Gui.evCloseNowB");
            Class.forName("Gui.evLockA");
            Class.forName("Gui.evLockB");
            Class.forName("Gui.evUnlockA");
            Class.forName("Gui.evUnlockB");
    
        // Registering Static Classes 
        
      }
      catch(Exception e) { 
         java.lang.System.err.println(e.toString());
         e.printStackTrace(java.lang.System.err);
      }
    }
    //#]
    
    protected static ElevatorGui p_ElevatorGui = null;
    
    //## configuration DefaultComponent::DefaultConfig 
    public static void main(String[] args) {
        RiJOXF.Init(null, 0, 0, true, args);
        MainDefaultComponent initializer_DefaultComponent = new MainDefaultComponent();
        p_ElevatorGui = new ElevatorGui(RiJMainThread.instance());
        p_ElevatorGui.startBehavior();
        //#[ configuration DefaultComponent::DefaultConfig 
        //#]
        RiJOXF.Start();
        p_ElevatorGui=null;
    }
    
}
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/MainDefaultComponent.java
*********************************************************************/

